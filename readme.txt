﻿Postaviti db.class za spajanje na bazu
Pokrenuti prepare_DB


ZADATAK:
Registrirani korisnici mogu komentirati i ocjenjivati filmove, pretraživati filmove po žanru, glumcu,
godini itd. Korisnik također može dobiti popis top-rated filmova (po žanru, godini itd.) i kreirati svoju listu filmova
koje želi pogledati, na kojoj kasnije može označiti filmove koje je pogledao. Korisniku je
dostupan pregled svih njegovih ocjena filmova. Administrator može dodavati nove filmove, brisati
neprimjerene komentare korisnika i, u slučaju učestalog neprimjerenog ponašanja, obrisati korisnikov račun.


LUCIJA: prepare_DB popravak, service napraviti funkcije(imati fukciju koja pamti score od filma,
prosjećna vrijednost scorova svih), prvih 8

TINA: log-in(na login.php je to; projectrp2 se zove cimdb(clone internet movie database)?)
        i registracija (na posebnoj stranici)

JELENA: pola funkcija, drugih 8


Fukcije za bazu koje imamo:

SVE VRAĆA CIJELU KLASU

function markAsBad($user_id) - Funckija danom korisniku povisuje warning_flag za jedan
function getMoviesByDirector($director_id)  - Funkcija vraća film za danog redatelja
function getMoviesByYear($year) - Funkcija vraća film za danu godinu
function getMovieByGenre($genre) - Funkcija vraća film za dani žanr
function getMoviesByActor($actor_id) - Vraća listu filmova u kojima je glumio dani glumac. Vraća se lista objekata tipa movies
function getActorById($id) - Funckija vraća glumca, baš cijeli objekt tipa actor. Koji glumac će biti vraćen ovisi o predanom idu
function getActorsByMovie($movie_id) - Vraća sve glumce koji su glumili u nekom filmu koji je dan svom idom
function getMovieById($id) - Vraća film za traženi id
function getWishlist($user_id) - Vraća wishlistu od danog korisnika. Vraćena vrijednost je polje koje sadrži objekte tipa film, ne samo nazive filmova
function putComment($user_id, $movie_id, $comment, $score) - Funckija stavlja komentar u bazu podataka, i na temelju dane ocjene, računa novu prosječnu ocjenu 
							za film, i ona postaje score za taj film

function deleteUser($user_id) - Brise korisnika iz tablice project_users, brise sve njegove komentare iz tablice project_comments i brise njegov popis filmova 
				iz tablice project_wishlist.
function getDirectorByMovie($movie_id) - Vraca klasu Director, redatelja danog filma. U tablici project_movies za dani movie_id izvlaci director_id, iz tablice 
					project_directors dohvaća sve inf o redatlju. 
function putInWishList($user_id, $movie_id) - Dodaje dani film u wishlist danog korisnika. 
function markAsWatched($user_id, $movie_id) - Oznacuje dani film kao pogledan.
function getMyComments($user_id) - Vraca listu class Comment, sve komentare danog korisnika.
function getMovieComments($movie_id) - Vraca listu class Comments, komentare za dani film.
function changeMovieScore($movie_id, $score) - Mijenja score filma s danim score-om.
function deleteComment($comment_id) - U tablici project_comments brise dani komentar.
function check_login($username, $password) -Provjerava jesu li username i password točni za login.
function check_seq($seq) - Za registraciju novog korisnika.
function check_registration($username, $password, $name, $surname, $email) -Za registraciju novog korisnika.
function getRole($actor_id, $movie_id) - Vraća ime ulogu za zadani id glumca i id filma.
function getUserById($user_id) - Vraća usera za zadani id.
function getAllUser($user_id) - Vraća popis svih usera.


Organizacija podataka:


FILMOVI:
id, name, director_id, genre, year, score

GLUMCI:
id, name, surname, birth_y, death_y

KORISNICI:
id, username, pass_hash, name, surname, warning_flag, admin_flag

POVEZNICA GLUMACA I FILMOVA:
id, actor_id, movie_id, character

WISHLIST:
id, user_id, movie_id, status

REDATELJ:
id, name, surname

KOMENTARI:
id, user_id, movie_id, comment, score

Ovdje možemo zapisivati što je do sad napravljeno:
- 16 početnih funkcija + 2 Lucijine pomoćne funkcije
- login/register controlleri i 4 view-a za login i registraciju

ŠTO DALJE:
T dodati kratke opise filmove - u prepareDB i svugdje gdje treba
T nakon logina prebaciti se na home.php
view za:
   T - navigation bar - view/navigation.php
           L - toprated-->view/movie_list.php++, J myprofile-->view/myprofile.php, L search-->view/search.php++
   L - popis filmova (općeniti) - view/movie_list.php++
   T - početnu stranicu -> za sad prazna, samo navigation bar - view/home.php
   L - komentiranje / ocjenjivanje filmova - view/comment.php++
   J - korisnički profil - sadrži osnovne informacije i navigaciju za svoje komentare i wishlist
                (wishlist koristi view za popis filmova)
                view/myprofile.php
   T - prikaz filma view/movie.php
   L - prikaz glumca view/actor.php (Tina napravila)++
   L - prikaz redatelja view/director.php++
   J - prikaz svih usera (samo za administratora) view/user_list.php
   J - tuđi korisnički profil - sadrži osnovne informacije i pregled svih komentara
                view/userprofile.php
   L - logout i homepage button ++





