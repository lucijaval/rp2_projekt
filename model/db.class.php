<?php

/**
 * Razred DB koji služi za konekciju na bazu podataka
 */
class   DB
{
    /**
     * @var konekcija
     */
    private static $db = null;

    /**
     * DB constructor.
     */
    private function __construct()
    {
    }

    /**
     * Klon
     */
    private function __clone()
    {
    }

    /**
     * Stvara i vraća konekciju
     * @return konekcija
     */
    public static function getConnection()
    {
        if (DB::$db === null) {
            try {
                // Unesi ispravni HOSTNAME, DATABASE, USERNAME i PASSWORD
                DB::$db = new PDO("mysql:host=rp2;dbname=kriz; charset=utf8", 'student', 'pass.mysql');
                DB::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                exit('PDO Error: ' . $e->getMessage());
            }
        }
        return DB::$db;
    }
}
