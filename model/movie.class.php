<?php

/**
 * Razred Movie koji opisuje film
 */
class Movie
{
    /**
     * Varijable redom
     * id filma
     * naslov filma
     * redatelj filma (id)
     * godina
     * žanr
     * opis
     * ocjena
     * link na trailer
     * slika
     */
    protected $id, $title, $director_id, $year, $genre, $description, $score, $link, $photo;

    /**
     * Konstruktor
     * Movie constructor.
     * @param $id
     * @param $title
     * @param $director_id
     * @param $year
     * @param $genre
     * @param $description
     * @param $score
     * @param $link
     * @param $photo
     */
    public function __construct($id, $title, $director_id, $year, $genre, $description, $score, $link, $photo)
    {
        $this->id = $id;
        $this->title = $title;
        $this->director = $director_id;
        $this->year = $year;
        $this->genre = $genre;
        $this->description = $description;
        $this->score = $score;
        $this->link = $link;
        $this->photo = $photo;
    }

    /**
     * Getter
     * @param $prop
     * @return mixed
     */
    function __get( $prop ) { return $this->$prop; }

    /**
     * Setter
     * @param $prop
     * @param $val
     * @return $this
     */
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>
