<?php

require_once 'db.class.php';
require_once 'user.class.php';
require_once 'movie.class.php';
require_once 'director.class.php';
require_once 'comment.class.php';
require_once 'actor.class.php';
require_once 'wishlist.class.php';

class CimDbService{

  /**
   *Opominje usera, povecava mu warning flag.
   */
   function warnUser($user_id){
     try{
       $db = DB::getConnection();
       $st1 = $db->prepare('SELECT warning_flag FROM project_users WHERE id=:id');
       $st1->execute(array('id'=>$user_id));
       $flag_value = $st1->fetch()['warning_flag'];
       $flag_value++;
     }
     catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

     try{
       $st2 = $db->prepare( 'UPDATE project_users SET warning_flag=:warn WHERE id=:id' );
       $st2->execute(array('id'=>$user_id, 'warn'=>$flag_value));
     }
     catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

     return;
   }

    /**
     * Funkcija vraća film za danog redatelja
     * @param $director_id
     * @return array
     */
    function getMoviesByDirector($director_id){

        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT id, title, director_id, year, genre, description, score, link, photo FROM project_movies WHERE director_id=:director_id' );
            $st->execute( array( 'director_id' => $director_id) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();

        while($row = $st->fetch()){
            $arr[] = new Movie($row['id'], $row['title'], $row['director_id'], $row['year'], $row['genre'], $row['description'], $row['score'], $row['link'], $row['photo']);
        }

        return $arr;
    }

    /**
    * Vraća link od trailera od filma
    * @param $id
    * @return array
    */
   function getAllLinksOfMovies(){
       try
       {
           $db = DB::getConnection();
           $st = $db->prepare( 'SELECT id, link FROM project_media_movies' );
           $st->execute( );
       }
       catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

       $arr = array();

       while( $row = $st->fetch() ){

           $arr[] = new Media_movies($row['id'], $row['link']);
       }


       return $arr;

   }

    /**
     * Funkcija vraća film za danu godinu
     * @param $year
     * @return array
     */
    function getMoviesByYear($year){

        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT id, title, director_id, year, genre, description, score, link, photo FROM project_movies WHERE year=:year' );
            $st->execute( array( 'year' => $year) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();

        while($row = $st->fetch()){
            $arr[] = new Movie($row['id'], $row['title'], $row['director_id'], $row['year'], $row['genre'], $row['description'], $row['score'], $row['link'], $row['photo']);
        }

        return $arr;
    }

    /**
     * Funkcija vraća film za dani žanr
     * @param $genre
     * @return array
     */
    function getMovieByGenre($genre){

        $movies = $this->getAllMovies();

        $arr = array();
        foreach($movies as $m){
            $genres = explode(",", $m->genre);

            foreach($genres as $g){
                similar_text(trim($g), $genre, $perc1);
                if($perc1 >= 70){
                    $arr[] = $m;
                    break;
                }
            }


        }

        return $arr;
    }

    /**
     * Vraća listu filmova u kojima je glumio dani glumac.
     * Vraća se lista objekata tipa movies
     * @param $actor_id
     * @return array
     */
    function getMoviesByActor($actor_id){

        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT movie_id FROM project_acting WHERE actor_id=:actor_id' );
            $st->execute( array( 'actor_id' => $actor_id) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();

        while($row = $st->fetch()){
            $arr[] = $this->getMovieById($row['movie_id']);
        }

        return $arr;
    }

    /**
     *
     * Funckija vraća glumca, baš cijeli objekt tipa actor. Koji glumac
     * će biti vraćen ovisi o predanom idu
     * @param $id
     * @return Actor|null
     */
    function getActorById($id){

        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT id, name, surname, birth_y, death_y, sex, bio, photo FROM project_actors WHERE id=:id' );
            $st->execute( array( 'id' => $id ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }


        $row = $st->fetch();

        if( $row === false ) {
            return null;
        }else{
            return new Actor($row['id'], $row['name'], $row['surname'], $row['birth_y'], $row['death_y'], $row['sex'], $row['bio'], $row['photo']);
        }

    }

    /**
     *
     * Vraća sve glumce koji su glumili u nekom filmu koji je dan svom
     * idom
     * @param $movie_id
     * @return array
     */
    function getActorsByMovie($movie_id){

        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT actor_id FROM project_acting WHERE movie_id=:movie_id' );
            $st->execute( array( 'movie_id' => $movie_id) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();

        while($row = $st->fetch()){
            $arr[] = $this->getActorById($row['actor_id']);
        }

        return $arr;


    }



    /**
     * Vraća redatelja za traženi id
     *
     * @param $movie_id
     * @return Movie|null
     */
    function getDirectorById($id){
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT id, name, surname, sex, birth_y, death_y, bio, photo FROM project_directors WHERE id=:id' );
            $st->execute( array( 'id' => $id ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $row = $st->fetch();

        if($row == false){
            return null;
        }else{
            return new Director( $row['id'], $row['name'], $row['surname'], $row['sex'], $row['birth_y'], $row['death_y'], $row['bio'], $row['photo'] );
        }

    }

    /**
     * Vraća redatelja po imenu i prezimenu, onaj čije je ime i prezime
     * najsličnije poslanom
     * @param $name
     * @param $surname
     * @return mixed
     */
    function getDirectorByName($name, $surname){

        $directors = $this->getAllDirectors();

        foreach($directors as $d){

            similar_text($d->name, $name, $perc1);
            similar_text($d->surname, $surname, $perc2);

            if($perc1 >= 70 && $perc2 >= 70){
                return $d;
            }

            similar_text($d->name, $surname, $perc1);
            similar_text($d->surname, $name, $perc2);

            if($perc1 >= 70 && $perc2 >= 70){
                return $d;
            }
        }

    }

    /**
     * Vraća glumca po imenu i prezimenu, onaj čije je ime i prezime
     * najsličnije poslanom
     * @param $name
     * @param $surname
     * @return mixed
     */
    function getActorByName($name, $surname){

        $actors = $this->getAllActors();

        foreach($actors as $a){

            similar_text($a->name, $name, $perc1);
            similar_text($a->surname, $surname, $perc2);

            if($perc1 >= 70 && $perc2 >= 70){
                return $a;
            }

            similar_text($a->name, $surname, $perc1);
            similar_text($a->surname, $name, $perc2);

            if($perc1 >= 70 && $perc2 >= 70){
                return $a;
            }
        }

    }



    /**
     * Vraća film za traženi id
     *
     * @param $movie_id
     * @return Movie|null
     */
    function getMovieById($id){
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT id, title, director_id, year, genre, description, score, link, photo FROM project_movies WHERE id=:id' );
            $st->execute( array( 'id' => $id ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $row = $st->fetch();

        if($row == false){
            return null;
        }else{
            return new Movie($row['id'], $row['title'], $row['director_id'], $row['year'], $row['genre'], $row['description'], $row['score'], $row['link'], $row['photo']);
        }

    }


    /**
     * Vraća film čiji je naziv najsličniji poslanom naslovu
     * @param $name
     * @return array
     */
    function getMoviesByName($name){

        $movies = $this->getAllMovies();
        $arr = array();

        foreach($movies as $movie){

            similar_text($movie->title, $name, $perc1);
            similar_text($name, $movie->title, $perc2);

            if($perc1 >= 60 || $perc2 >= 60){
                $arr[] = $movie;
            }

        }

        return $arr;

    }

    /**
     * Vraća wishlistu od danog korisnika. Vraćena vrijednost je
     * polje koje sadrži objekte tipa film, ne samo nazive filmova
     *
     * @param $user_id
     * @return array
     */
    function getWishlist($user_id){

        try{
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT movie_id FROM project_wishlist WHERE user_id=:user_id' );
            $st->execute(array('user_id'=>$user_id));
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();

        while( $row = $st->fetch() ){

            $arr[] = $this->getMovieById($row['movie_id']);
        }

        return $arr;

    }

    /**
     * Vraca listu wishlista. Elementi liste su class Wishlist.
     */
    function getWishes($user_id){

        try{
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT * FROM project_wishlist WHERE user_id=:user_id' );
            $st->execute(array('user_id'=>$user_id));
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();

        while( $row = $st->fetch() ){
            $arr[] = new Wishlist( $row['id'], $row['user_id'], $row['movie_id'], $row['status']);
        }

        return $arr;

    }

    /**
     * Promjeni status filma, da li je pogledan
     * ili ne
     * @param $user_id
     * @param $movie_id
     * @param $status
     */
    function updateStatus($user_id, $movie_id, $status){

      try{
        $db = DB::getConnection();
        $st = $db->prepare( 'UPDATE project_wishlist SET status=:status WHERE user_id=:user_id AND movie_id=:movie_id' );
        $st->execute(array('status'=>$status, 'user_id'=>$user_id, 'movie_id'=>$movie_id));
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      return;
    }

    /**
     * Makni jedan film iz wish liste
     * @param $user_id
     * @param $movie_id
     */
    function removeWish($user_id, $movie_id){

      try{
        $db = DB::getConnection();
        $st = $db->prepare( 'DELETE FROM project_wishlist WHERE user_id=:user_id AND movie_id=:movie_id' );
        $st->execute(array('user_id'=>$user_id, 'movie_id'=>$movie_id));
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      return;
    }



    /**
     * Funckija stavlja komentar u bazu podataka, i na temelju dane ocjene,
     * računa novu prosječnu ocjenu za film, i ona postaje score za taj film
     *
     * @param $user_id
     * @param $movie_id
     * @param $comment
     * @param $score
     */
    function putComment($user_id, $movie_id, $comment, $score, $com_date){
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'INSERT INTO project_comments(user_id, movie_id, comment, score, com_date) VALUES (:user_id, :movie_id, :comment, :score, :com_date)' );
            $st->execute( array( 'user_id' => $user_id, 'movie_id' => $movie_id, 'comment' => $comment, 'score' => $score, 'com_date' => $com_date) );
        }
        catch( PDOException $e ) { exit( "PDO error [project_comments]: " . $e->getMessage() ); }

        $comments = $this->getMovieComments($movie_id);
        $len = sizeof($comments);
        $newScore = 0;

        foreach ($comments as $c){
            $newScore += $c->score;
        }

        $this->changeMovieScore($movie_id, ($newScore / $len));
    }

     /**
      * Brise korisnika iz tablice project_users, brise sve njegove komentare iz tablice
      * project_comments i brise njegov popis filmova iz tablice project_wishlist.
      */
    function deleteUser($user_id){

        try{
			$db = DB::getConnection();
            $st1 = $db->prepare( 'DELETE FROM project_users WHERE id=:id' );
            $st2 = $db->prepare( 'DELETE FROM project_comments WHERE user_id=:user_id' );
            $st3 = $db->prepare( 'DELETE FROM project_wishlist WHERE user_id=:user_id' );

            $st1->execute(array('id'=>$user_id));
            $st2->execute(array('user_id'=>$user_id));
            $st3->execute(array('user_id'=>$user_id));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        return;
    }

    /**
     * Vraca klasu Director, redatelja danog filma.
     * U tablici project_movies za dani movie_id izvlaci director_id, iz tablice project_directors
     * dohvaća sve inf o redatlju.
     */
    function getDirectorByMovie($movie_id){

        try{
			$db = DB::getConnection();
			$st1 = $db->prepare( 'SELECT id, director_id FROM project_movies WHERE id=:id' );
            $st1->execute(array('id'=>$movie_id));
            $director_id = $st1->fetch()['director_id'];

            $st2 = $db->prepare( 'SELECT * FROM project_directors WHERE id=:id' );
            $st2->execute(array('id'=>$director_id));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $row = $st2->fetch();
        if($row == false){
            return null;
        }else{
            return new Director( $row['id'], $row['name'], $row['surname'], $row['sex'], $row['birth_y'], $row['death_y'], $row['bio'], $row['photo'] );
        }

    }

    /**
     * Dodaje dani film u wishlist danog korisnika.
     * Dodaje u project_wishlist novi element s danim user_id-o i move_id te
     * postavlja status na 0.
     */
    function putInWishList($user_id, $movie_id){

        try{
			$db = DB::getConnection();
			$st = $db->prepare( 'INSERT INTO project_wishlist(user_id, movie_id, status) VALUES (:user_id, :movie_id, :status)');
			$st->execute(array('user_id'=>$user_id, 'movie_id'=>$movie_id, 'status'=>0));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        return;
    }

    /**
     * Oznacuje dani film kao pogledan.
     * U tablici project_wishlist mijenja status u 1.
     */
    function markAsWatched($user_id, $movie_id){

        try{
			$db = DB::getConnection();
			$st = $db->prepare( 'UPDATE project_wishlist SET status=1 WHERE user_id=:user_id AND movie_id=:movie_id' );
			$st->execute(array('user_id'=>$user_id, 'movie_id'=>$movie_id));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        return;
    }

    /**
     * Vraca listu class Comment, sve komentare danog korisnika.
     * U tablici project_commnets izvlaci sve komentare koji imaju user_id jednak danom user_id-u.
     */
    function getMyComments($user_id){

        try{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT * FROM project_comments WHERE user_id=:user_id' );
			$st->execute(array('user_id'=>$user_id));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$arr = array();
		while( $row = $st->fetch() ){
			$arr[] = new Comment( $row['id'], $row['user_id'], $row['movie_id'], $row['comment'], $row['score'], $row['com_date'] );
		}

		return $arr;
    }

    /**
     * Vraca listu class Comments, komentare za dani film.
     * U tablici project_commnets izvlaci sve komentare koji kao movie_id imaju dani movie_id.
     */
    function getMovieComments($movie_id){

        try{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT * FROM project_comments WHERE movie_id=:movie_id' );
			$st->execute(array('movie_id'=>$movie_id));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$arr = array();
		while( $row = $st->fetch() ){
			$arr[] = new Comment( $row['id'], $row['user_id'], $row['movie_id'], $row['comment'], $row['score'], $row['com_date'] );
		}

		return $arr;
    }

    function cmp($a, $b){
        return $a->score < $b->score;
    }

    function getTopTenMovies(){

        $movies = $this->getAllMovies();

        usort($movies, array($this, "cmp"));

        $arr = array();

        for($i = 0; $i < 10; $i++){
            $arr[] = $movies[$i];
        }

        return $arr;
    }


    /**
     * Mijenja score filma s danim score-om.
     * U tablici project_movies za dani movie_id mijenja score.
     */
    function changeMovieScore($movie_id, $score){

        try{
			$db = DB::getConnection();
			$st = $db->prepare( 'UPDATE project_movies SET score=:score WHERE id=:id' );
			$st->execute(array('id'=>$movie_id, 'score'=>$score));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		return;
    }

    /**
     * U tablici project_comments brise dani komentar.
     */
    function deleteComment($user_id, $date, $movie_id){

        try{
			$db = DB::getConnection();
            $st = $db->prepare( 'DELETE FROM project_comments WHERE user_id=:id AND com_date=:comment_date' );
			$st->execute(array('id'=>$user_id, 'comment_date'=>$date));
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $comments = $this->getMovieComments($movie_id);
        $len = sizeof($comments);
        $newScore = 0;

        foreach ($comments as $c){
            $newScore += $c->score;
        }

        $this->changeMovieScore($movie_id, ($newScore / $len));
        return;
    }


     /**
     * Provjerava jesu li username i password točni za login.
     * Vraća array (false, false) ako nešto ne valja, (user_id, username) ako je sve u redu.
     */
    function check_login($username, $password) {


        try{
            $db = DB::getConnection();

            $st=$db->prepare('SELECT id, username, password_hash, has_registered FROM project_users WHERE username=:username');

            $st->execute(array('username' => $username));

        }catch(PDOException $ex){
            exit('Error');
        }

        if($st->rowCount()!==1){

            return array(false, false);}
        $row=$st->fetch();
        if($row['has_registered'] != 1){

            return array(true, false);
        }
        if(password_verify($password, $row['password_hash'])){

            return array($row['id'], $username);

        }
        else {
            return array(false, false);
        }
    }

    /**
     * Za registraciju novog korisnika.
     * Ova f-ja analizira $_GET['seq'] i u bazi postavlja has_registered=1 za onog korisnika koji ima taj niz.
     * Vraća poruku s greškom ili all_good ako je sve u redu.
     */

    function check_seq($seq) {

        if( !preg_match( '/^[a-z]{20}$/', $seq ) )
            return 'Something wrong with registration sequence.' ;

        // Nađi korisnika s tim nizom u bazi
        $db = DB::getConnection();

        try
        {
            $st = $db->prepare( 'SELECT * FROM project_users WHERE reg_seq=:reg_seq' );
            $st->execute( array( 'reg_seq' => $seq ) );
        }
        catch( PDOException $e ) { exit( 'Greška u bazi: ' . $e->getMessage() ); }

        $row = $st->fetch();

        if( $st->rowCount() !== 1 )
            return 'Registration sequence in use by ' . $st->rowCount() . 'users.' ;
        else
        {
            // Sad znamo da je točno jedan takav. Postavi mu has_registered na 1.
            try
            {
                $st = $db->prepare( 'UPDATE project_users SET has_registered=1 WHERE reg_seq=:reg_seq' );
                $st->execute( array( 'reg_seq' => $seq ) );
            }
            catch( PDOException $e ) { exit( 'Greška u bazi: ' . $e->getMessage() ); }

            // Sve je uspjelo, zahvali mu na registraciji.
            return 'all_good';
        }
    }
    /**
     * Za registraciju novog korisnika.
     * Provjerava postoji li već takav korisnik u bazi i šalje mail za potvrdu registracije.
     * Vraća poruku s opisom greške ili good ako je sve u redu.
     */
    function check_registration($username, $password, $name, $surname, $email){
        // Provjeri jel već postoji taj korisnik u bazi
        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_users WHERE username=:username');
            $st->execute(array('username' => $username));
        } catch (PDOException $e) {
            exit('Greška u bazi: ' . $e->getMessage());
        }

        if ($st->rowCount() !== 0)
            return 'Username already in use. Please select different username.';

        // Dakle sad je napokon sve ok.
        // Dodaj novog korisnika u bazu. Prvo mu generiraj random string od 10 znakova za registracijski link.
        $reg_seq = '';
        for ($i = 0; $i < 20; ++$i)
            $reg_seq .= chr(rand(0, 25) + ord('a')); // Zalijepi slučajno odabrano slovo

        try {
            $st = $db->prepare('INSERT INTO project_users(username, password_hash, name, surname, email, reg_seq, has_registered, warning_flag, admin_flag) VALUES ' .
                '(:username, :password, :name, :surname, :email, :reg_seq, 0, 0, 0)');

            $st->execute(array('username' => $username,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'email' => $email,
                'name' => $name,
                'surname' => $surname,
                'reg_seq' => $reg_seq));
        } catch (PDOException $e) {
            exit('Greška u bazi: ' . $e->getMessage());
        }

        // Sad mu još pošalji mail
        $to = $email;
        $subject = 'Registration mail';
        $message = 'Dear ' . $username . "!\nTo finish your registration process please click on the following link: ";
        $message .= 'http://' . $_SERVER['SERVER_NAME'] . htmlentities(dirname($_SERVER['PHP_SELF'])) . '/cimdb.php?rt=register/check&seq=' . $reg_seq . "\n";
        $headers = 'From: rp2@studenti.math.hr' . "\r\n" .
            'Reply-To: rp2@studenti.math.hr' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $isOK = mail($to, $subject, $message, $headers);

        if (!$isOK)
            return 'Can\'t send email.';

        // Zahvali mu na prijavi.
        return 'good';
    }

    /**
     * Vraća ime ulogu za zadani id glumca i id filma.
     * Ako glumac nije glumio u tom filmu vraća false.
     */
    function getRole($actor_id, $movie_id)
    {
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT role FROM project_acting WHERE actor_id=:actor_id AND movie_id=:movie_id' );
            $st->execute( array( 'actor_id' => $actor_id, 'movie_id' => $movie_id ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $row = $st->fetch();

        if($row == false){
            return false;
        }else{
            return $row['role'];
        }
    }

    /**
     * Vraća usera za zadani id.
     */
    function getUserById($user_id)
    {
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT * FROM project_users WHERE id=:id' );
            $st->execute( array( 'id' => $user_id ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $row = $st->fetch();

        if($row == false){
            return null;
        }else{
            return new User($row['id'], $row['username'], $row['password_hash'], $row['name'], $row['surname'], $row['email'], $row['warning_flag'], $row['admin_flag'], $row['has_registered']);
        }

    }

    /**
     * Vraća popis svih usera.
     */
    function getAllUser($user_id)
    {
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT * FROM project_users' );
            $st->execute();
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();
        while( $row = $st->fetch() ){
			$arr[] = new User($row['id'], $row['username'], $row['password_hash'], $row['name'], $row['surname'], $row['email'], $row['warning_flag'], $row['admin_flag'], $row['has_registered']);
        }

        return $arr;
    }


    /**
     * Vraća listu svih filmova
     * @return array
     */
    function getAllMovies()
    {
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT * FROM project_movies' );
            $st->execute();
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();
        while( $row = $st->fetch() ){
            $arr[] = new Movie($row['id'], $row['title'], $row['director_id'], $row['year'], $row['genre'], $row['description'], $row['score'], $row['link'], $row['photo']);
        }

        return $arr;
    }


    /**
     * Vraća listu svih redatelja
     * @return array
     */
    function getAllDirectors()
    {
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT * FROM project_directors' );
            $st->execute();
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();
        while( $row = $st->fetch() ){
            $arr[] = new Director( $row['id'], $row['name'], $row['surname'], $row['sex'], $row['birth_y'], $row['death_y'], $row['bio'], $row['photo']);
        }

        return $arr;
    }


    /**
     * Vraća listu svih glumaca
     * @return array
     */
    function getAllActors()
    {
        try
        {
            $db = DB::getConnection();
            $st = $db->prepare( 'SELECT * FROM project_actors' );
            $st->execute();
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $arr = array();
        while( $row = $st->fetch() ){
            $arr[] =  new Actor($row['id'], $row['name'], $row['surname'], $row['sex'], $row['birth_y'], $row['death_y'], $row['bio'], $row['photo']);
        }

        return $arr;
    }


    /**
     * Ubaci u bazu novog glumca i vrati ga.
     */
    function addActor($name, $surname, $sex, $birth_y, $death_y, $bio, $photo) {
      if($death_y == 0) $death_y = NULL;
      try
      {
          $db = DB::getConnection();
          $st = $db->prepare( 'INSERT INTO project_actors(name, surname, sex, birth_y, death_y, bio, photo) VALUES (:name, :surname, :sex, :birth_y, :death_y, :bio, :photo)'  );
          $st->execute(array( 'name' => $name, 'surname' => $surname, 'sex' => $sex, 'birth_y' => $birth_y, 'death_y' => $death_y, 'bio' => $bio, 'photo' => $photo ));
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      try
      {
          $db = DB::getConnection();
          $st1 = $db->prepare( 'SELECT * FROM project_actors ORDER BY id DESC LIMIT 1');
          $st1->execute();
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      $arr = array();
      while( $row = $st1->fetch() ){
          $arr[] =  new Actor($row['id'], $row['name'], $row['surname'], $row['birth_y'], $row['death_y'], $row['sex'], $row['bio'], $row['photo']);
      }

      return $arr[0];
    }

    /**
     * Ubaci u bazu novog redatelja i vrati ga.
     */
    function addDirector($name, $surname, $sex, $birth_y, $death_y, $bio, $photo) {
      if($death_y == 0) $death_y = NULL;
      try
      {
          $db = DB::getConnection();
          $st = $db->prepare( 'INSERT INTO project_directors(name, surname, sex, birth_y, death_y, bio, photo) VALUES (:name, :surname, :sex, :birth_y, :death_y, :bio, :photo)'  );
          $st->execute(array( 'name' => $name, 'surname' => $surname, 'sex' => $sex, 'birth_y' => $birth_y, 'death_y' => $death_y, 'bio' => $bio, 'photo' => $photo ));
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      try
      {
          $db = DB::getConnection();
          $st1 = $db->prepare( 'SELECT * FROM project_directors ORDER BY id DESC LIMIT 1');
          $st1->execute();
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      $arr = array();
      while( $row = $st1->fetch() ){
          $arr[] =  new Director($row['id'], $row['name'], $row['surname'], $row['sex'], $row['birth_y'], $row['death_y'], $row['bio'], $row['photo']);
      }

      return $arr[0];
    }


    /**
     * Ubaci u bazu novi film i vrati ga.
     */
    function addMovie($title, $year, $genre, $photo, $link, $actorsList, $rolesList, $director, $description) {
      try
      {
          $db = DB::getConnection();
          $st = $db->prepare( 'INSERT INTO project_movies(title, director_id, year, genre, description, score, link, photo) VALUES (:title, :director_id, :year, :genre, :description, :score, :link, :photo)');
          $st->execute(array( 'title' => $title, 'director_id' => $director->id, 'year' => $year, 'genre' => $genre, 'description' => $description, 'score' => 0, 'link' => $link, 'photo' => $photo ));
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      try
      {
          $db = DB::getConnection();
          $st1 = $db->prepare( 'SELECT * FROM project_movies ORDER BY id DESC LIMIT 1');
          $st1->execute();
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      $arr = array();
      while( $row = $st1->fetch() ){
          $arr[] =  new Movie($row['id'], $row['title'], $row['director_id'], $row['year'], $row['genre'], $row['description'], $row['score'], $row['link'], $row['photo']);
      }

      //dodavanje uloga:
      try
      {
          $db = DB::getConnection();
          $st = $db->prepare( 'INSERT INTO project_acting(actor_id, movie_id, role) VALUES (:actor_id, :movie_id, :role)');
          for($i = 0; $i < 4; ++$i)
              $st->execute(array( 'actor_id' => $actorsList[$i]->id, 'movie_id' => $arr[0]->id, 'role' => $rolesList[$actorsList[$i]->id]));
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      return $arr[0];
    }

    /**
     * Funckija vraća sve glumce čije ime počinje sa poslanim stringom
     * "name"
     * @param $name
     * @return array
     */
    function getActorStartingWith($name){

        $arr = $this->getAllActors();

        $response = array();

        for($i = 0, $n = sizeof($arr); $i < $n; $i++){

            $a = lcfirst($arr[$i]->name)." ".lcfirst($arr[$i]->surname);

            if(is_int(strpos($a, strtolower($name)))){

                $response[] = $arr[$i];
            }
        }

        return $response;

    }


    /**
     * Funckija vraća sve redatelje čije ime počinje sa poslanim stringom
     * "name"
     * @param $name
     * @return array
     */
    function getDirectorStartingWith($name){

        $arr = $this->getAllDirectors();

        $response = array();

        //substr ( string $string , int $start [, int $length ] ) : string
        for($i = 0; $i < sizeof($arr); $i++){

            $d = lcfirst($arr[$i]->name)." ".lcfirst($arr[$i]->surname);
            if(is_int(strpos($d, strtolower($name)))){
                $response[] = $arr[$i];
            }
        }
        return $response;

    }

    /**
     * Vraća listu filmova koji u svom nazivu
     * imaju poslan string
     * @param $searchBar
     * @return array
     */
    function getMoviesStartingWith($searchBar){

        $arr = $this->getAllMovies();
        $response = array();

        for($i = 0; $i < sizeof($arr); $i++){

            $m = $arr[$i] ->title;
            if( is_int(strpos(strtolower($m), strtolower($searchBar)))) {
                // echo 'da';
                $response[] = $arr[$i];
            }
        }

        return $response;
    }
}

?>
