<?php

/**
 * Razred Director koji opisuje redatelja
 */
class Director
{
    /**
     * Varijable redom
     * id redatelja
     * ime redatelja
     * prezime redatelja
     * spol redatelja
     * godina rođenja
     * godina smrti
     * opis
     * slika
     */
    protected $id, $name, $surname, $sex, $birth_y, $death_y, $bio, $photo;

    /**
     * Konstruktor
     *
     * Director constructor.
     * @param $id
     * @param $name
     * @param $surname
     * @param $sex
     * @param $birth_y
     * @param $death_y
     * @param $bio
     * @param $photo
     */
    public function __construct($id, $name, $surname, $sex, $birth_y, $death_y, $bio, $photo)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->sex = $sex;
        $this->birth_y = $birth_y;
        $this->death_y = $death_y;
        $this->bio = $bio;
        $this->photo = $photo;
    }

    /**
     * Getter
     * @param $prop
     * @return mixed
     */
    function __get( $prop ) { return $this->$prop; }

    /**
     * Setter
     * @param $prop
     * @param $val
     * @return $this
     */
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>
