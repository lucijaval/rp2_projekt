<?php

/**
 * Razred User koji opisuje korisnika
 */
class User
{
    /**
     * Varijable redom
     * id korisnika
     * username
     * password hash
     * ime
     * prezime
     * email
     * warning flag
     * admin flag
     * flag has registered
     */
    protected $id, $username, $password_hash, $name, $surname, $email, $warning_flag, $admin_flag, $has_registered;

    /**
     * User constructor.
     * @param $id
     * @param $username
     * @param $password_hash
     * @param $name
     * @param $surname
     * @param $email
     * @param $warning_flag
     * @param $admin_flag
     * @param $has_registered
     */
    public function __construct($id, $username, $password_hash, $name, $surname, $email, $warning_flag, $admin_flag, $has_registered)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password_hash = $password_hash;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->admin_flag = $admin_flag;
        $this->warning_flag = $warning_flag;
        $this->has_registered = $has_registered;
    }

    /**
     * Getter
     * @param $prop
     * @return mixed
     */
    function __get( $prop ) { return $this->$prop; }

    /**
     * Setter
     * @param $prop
     * @param $val
     * @return $this
     */
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>