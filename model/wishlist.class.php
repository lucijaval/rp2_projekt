<?php

/**
 * Razred Wishlist koji opisuje wishlist
 */
class Wishlist
{
    /**
     * Varijable redom
     * id jedne želje u wish listi
     * korisnikov id
     * film id
     * status (pogledan/nepogledan)
     */
    protected $id, $user_id, $movie_id, $status;

    /**
     * Wishlist constructor.
     * @param $id
     * @param $user_id
     * @param $movie_id
     * @param $status
     */
    public function __construct($id, $user_id, $movie_id, $status)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->movie_id = $movie_id;
        $this->status = $status;
    }

    /**
     * Getter
     * @param $prop
     * @return mixed
     */
    function __get( $prop ) { return $this->$prop; }

    /**
     * Setter
     * @param $prop
     * @param $val
     * @return $this
     */
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>
