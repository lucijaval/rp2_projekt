<?php

/**
 * Razred Comment koji opisuje komentar
 */
class Comment
{
    /**
     * Varijable redom:
     * id komentara
     * id korisnika koji je ostavio komentar
     * id filma kod kojeg je komentar ostavljen
     * tekst komentara
     * ocjena filma
     * datum komentiranja
     */
    protected $id, $user_id, $movie_id, $comment, $score, $com_date;

    /**
     * Konstruktor
     * Comment constructor.
     * @param $id
     * @param $user_id
     * @param $movie_id
     * @param $comment
     * @param $score
     * @param $com_date
     */
    public function __construct($id, $user_id, $movie_id, $comment, $score, $com_date)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->movie_id = $movie_id;
        $this->comment = $comment;
        $this->score = $score;
        $this->com_date = $com_date;
    }

    /**
     * Getter
     * @param $prop
     * @return mixedGet
     */
    function __get( $prop ) { return $this->$prop; }

    /**
     * Setter
     * @param $prop
     * @param $val
     * @return $this
     */
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>