<?php

/**
 * Razred Actor, opisuje glumca
 */
class Actor
{
    /**
     * Redom varijable su:
     * id glumca
     * ime glumca
     * prezime glumca
     * godina rođenja
     * godina smrti
     * spol
     * opis
     * slika
     */
    protected $id, $name, $surname, $birth_y, $death_y, $sex, $bio, $photo;

    /**
     * Konstruktor
     *
     * Actor constructor.
     * @param $id
     * @param $name
     * @param $surname
     * @param $birth_y
     * @param $death_y
     * @param $sex
     * @param $bio
     * @param $photo
     */
    public function __construct($id, $name, $surname, $birth_y, $death_y, $sex, $bio, $photo)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->birth_y = $birth_y;
        $this->death_y = $death_y;
        $this->sex = $sex;
        $this->bio = $bio;
        $this->photo = $photo;
    }

    /**
     * Getter
     * @param $prop
     * @return mixed
     */
    function __get( $prop ) { return $this->$prop; }

    /**
     * Setter
     * @param $prop
     * @param $val
     * @return $this
     */
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>
