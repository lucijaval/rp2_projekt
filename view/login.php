<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <title>cIMDb</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="js/searchDirectors.js"></script>
    <script src="js/searchActors.js"></script>
    <script src="js/searchMain.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body id="h">
<form class="" action="cimdb.php?rt=login" method="post" id="loginf">
  <div class="imgcontainer">
      <img src="./images/user.png" alt="Avatar" class="avatar">
  </div>
  <div class="containeru">
    <label for="username"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="username" required id="tusername">

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>

    <button type="submit" name="login" id="loginbtn">Login</button>

</form>
<hr>
<form class="" action="cimdb.php?rt=register" method="post">
    <button type="submit" name="go_to_register" id="regist">Register</button>
</form>
</div>

<?php
require_once '_footer.html';
?>
