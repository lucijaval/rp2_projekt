<?php
require_once '_header.html';
require_once 'navigation.php';

echo'<script type="text/javascript" src="./js/myprofile.js"></script>';
echo "<h1>My profile </h1>";
echo '<div class="user"><img src="./images/user.png" alt="User" width="150" height="150"><span>';
echo '<b>'.$user->name . ' ' . $user->surname . '</b><br>';
echo '<b>Signed in as:</b> ' . $user->username . '<br>';

echo "<b>Status:</b> ";
if($user->admin_flag === "1"){
    echo "admin <br></span></div>";
}else{
    echo "user <br></span></div>";
}

echo "<hr><h3>My comments:</h3>";
$count = 0;
foreach($commentList as $comments){
  foreach ($movie as $m) {
    if($m->id === $comments->movie_id)
      $mo = $m;
  }
    echo '<input type="hidden" id="desc" name="desc" value="'.$mo->description.'">';
    echo '<div id="comment"><a id="tooltip" href="cimdb.php?rt=movie/show&id_movie=' . $comments->movie_id . '">' . $mo->title  . '</a><br>';
    echo '<div id="com">'.$comments->comment.'</div><br>';
    echo '<span class="fa fa-star fa-lg starchecked"></span> <b>' . $comments->score . '</b>/5<br>';
    echo '<small>Posted: '.$comments->com_date.'</small><br></div>';
}

if(sizeof($commentList) === 0)
    echo "No comments.";

echo "<hr><h3>My wish list:</h3>";
echo '<ul id="wish">';
foreach($wishList as $w){
    foreach($wish as $wi){
      if($wi->movie_id===$w->id){
        $status = $wi->status;
        if($status==="0")
          $status = "";
        else {
          $status = "checked";
        }
      }
    }
    echo '<li class="'.$status.'" id="'.$w->id.'">' . $w->title  . '</li>';
}
echo '</ul>';

if(sizeof($wishList) === 0)
    echo "No wishes.";

if($user->admin_flag === "1"){
    echo "<hr><h3>All users:</h3>";
    echo '<ul id="listuser">';
    foreach($users as $usr){
        echo '<li class="warning '.$usr->warning_flag.'" ><span id="' . $usr->id . '"><b>'.$usr->name.' '.$usr->surname.'</b> (<a href="cimdb.php?rt=profile/show&id_user=' . $usr->id . '">'. $usr->username . '</a>)</li>';
    }
    echo "</ul>";
}
?>
<?php
require_once '_footer.html';
?>
