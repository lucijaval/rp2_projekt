<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div id = "toppage">
  <h1 id = "pagetitle"> <span style="color:rgb(219, 187, 4);" class="fa fa-film"></span> cIMDb </h1>

<ul class = "navigation">
    <li class = "navigation"><a href="cimdb.php?rt=home"><i class="fa fa-home" aria-hidden="true"></i> Home page</a></li>
    <li class = "navigation"><a href="cimdb.php?rt=search"><i class="fa fa-search" aria-hidden="true"></i> Search</a></li>
    <li class = "navigation"><a href="cimdb.php?rt=movie"><i class="fa fa-star-o" aria-hidden="true"></i> Top Rated</a></li>
    <li class = "navigation"><a href="cimdb.php?rt=profile/myprofile"><i class="fa fa-user-circle" aria-hidden="true"></i> My Profile</a></li>
    <?php
    if($_SESSION['admin_flag'] === '1') echo '<li class = "navigation"><a href="cimdb.php?rt=movie/newMovie"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add new movie</a></li>';
    ?>
    <li class = "navigation" style="float:right"><a href="cimdb.php?rt=login"><i class="fa fa-sign-in" aria-hidden="true"></i> Log out</a></li>
</ul>
</div>

<div id = "bottompage">
