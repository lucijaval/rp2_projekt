<?php
require_once '_header.html';
require_once 'navigation.php';
?>

<script type="text/javascript" src="js/actor.js"></script>

<?php
echo '<h1>' . $actor->name . ' ' . $actor->surname . ' | <span id="profession">actor<span>'.'</h1>';
echo '<img id="actor_img" src="' . $actor->photo . '" alt="' . $actor->name . " " . $actor->surname . '">';
if(sizeof($moviesList)>0) echo '<iframe width="600" height="400" src="'. $moviesList[0]->link .'"> </iframe> ';

echo '<div class=age>Born: ' . $actor->birth_y;
if( $actor->death_y != null) {
    $age = (int)($actor->death_y) - (int)($actor->birth_y);
    echo '<br> Died: ' . $actor->death_y . ' (aged '   . $age . ')</div>';
}
else {
    $age = 2019 - (int)($actor->birth_y);
    echo ' (age '   . $age . ')</div>';
}

echo '<div class=bio>' . $actor->bio . '</div>';

 echo '<div class="known">';
 echo '<h3>Known for:</h3>';
foreach ($moviesList as $movie){
    echo '<a href="cimdb.php?rt=movie/show&id_movie=' . $movie->id . '">' . $movie->title . '</a>';
    echo ' as ' . $rolesList[$movie->id] . '<br>';
}
echo '</div>';
?>

<?php
require_once '_footer.html';
?>
