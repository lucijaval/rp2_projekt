<?php

require_once '_header.html';
require_once 'navigation.php';
echo '<div class="titleSearchlv">'.
    '<h1 class="mainSearchTitle" id="titleDirlv">Search directors</h1>'.
    '</div>';
?>


<form method="post" action="cimdb.php?rt=search/searchDirectors">
    <input type="text" name="nameDirector" placeholder="Write full name of director" id="txt3lv" list="list3lv">
    <input type="submit" name="search" value="Search!" class="searchlv"/>
</form>
<datalist id="list3lv"></datalist>

<nav class="otherSearchlv">
    <u class="inner" id="special3lv">MORE OPTIONS:</u>
    <a class="inner" href="cimdb.php?rt=search/mainSearch" id="alv">Return to main search!</a>
    <a class="inner" href="cimdb.php?rt=search/searchActors" id="alv">Search actors!</a>
    <?php
    if(isset($error)){
        echo '<u class="errorSearch" >'.$error.'</u>';
    }?>
</nav>

<?php
require_once '_footer.html';
?>
