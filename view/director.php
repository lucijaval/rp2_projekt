<?php
require_once '_header.html';
require_once 'navigation.php';
?>

<script type="text/javascript" src="js/actor.js"></script>

<?php
echo '<h1>' . $director->name . ' ' . $director->surname . ' | <span id="profession">director<span>'.'</h1>';
echo '<img id="actor_img" src="' . $director->photo . '" alt="' . $director->name . " " . $director->surname . '">';
if(sizeof($moviesList)>0) echo '<iframe width="600" height="400" src="'. $moviesList[0]->link .'"> </iframe> ';

echo '<div class=age>Born: ' . $director->birth_y;
if( $director->death_y != null) {
    $age = (int)($director->death_y) - (int)($director->birth_y);
    echo '<br> Died: ' . $director->death_y . ' (aged '   . $age . ')</div>';
}
else {
    $age = 2019 - (int)($director->birth_y);
    echo ' (age '   . $age . ')</div>';
}

echo '<div class=bio>' . $director->bio . '</div>';

echo '<div class="known">';
echo '<h3>Known for:</h3>';

foreach ($moviesList as $movie){
    echo '<a href="cimdb.php?rt=movie/show&id_movie=' . $movie->id . '">' . $movie->title . '</a>';
    echo '<br>';
}
echo '</div>';
?>


<?php
require_once '_footer.html';
?>
