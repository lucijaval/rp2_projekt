<?php
require_once '_header.html';
require_once 'navigation.php';

if(!isset($topTen)){
    echo '<h1>Search results</h1>';
}else{
    echo '<h1>Top Rated</h1>';
}

if(!isset($empty)){

    foreach($moviesList as $movie){

      echo '<h4><a href="cimdb.php?rt=movie/show&id_movie=' . $movie->id . '">' . $movie->title . '</a> (' . $movie->year . ')' ;
      echo '<input type="hidden" id="id_movie" name="id_movie" value="'.$movie->id.'">';
      echo ' <span class="movie_score"> <span class="fa fa-star starchecked" sytle="font-size: 10px;"></span> ' . $movie->score . '</span>/5</h4>';

    }

}else{
    echo 'No results found! Press button if you want to search again.';

}
if(!isset($topTen)){
    echo '<br><form method="post" action="cimdb.php?rt=search">';
    echo '<input type="submit" value="Searh again"/>';
    echo '</form>';
}

?>
<?php

require_once '_footer.html';
?>
