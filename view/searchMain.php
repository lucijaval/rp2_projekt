<?php

require_once '_header.html';
require_once 'navigation.php';

echo '<div class="titleSearchlv">'.
    '<h1 class="mainSearchTitle">Search movies</h1>'.
     '</div>';

?>

<form method="post" action="cimdb.php?rt=search/search">

<div id="lvdiv1">
<input type="text" name="nameThing" placeholder=" Choose what you want to search by and write title/name" list="list1lv" id="txt1lv"><?php echo'  ';?>
    <select name="searchBy" id="searchBy" >
        <option name="default" value="default">Choose...</option>
        <option name="year" value="year">Year</option>
        <option name="movieName" value="movie">Movie name</option>
        <option name="director" value="director">Director</option>
        <option name="actor" value="actor">Actor</option>
        <option name="genre" value="genre">Genre</option>

    </select>
</div>
    <div>
<input type="submit" name="search" value="Search!" class="searchlv"/>
    </div>


</form>
<datalist id="list1lv"></datalist>
<nav class="otherSearchlv">
<u class="inner" id="speciallv">SPECIALIZED SEARCH:</u>
<a class="inner" href="cimdb.php?rt=search/searchActors" id="alv">Search actors!</a>
<a class="inner" href="cimdb.php?rt=search/searchDirectors" id="alv">Search directors!</a>
    <?php
    if(isset($error)){
        echo '<u class="errorSearch" id="error2">'.$error.'</u>';
    }?>
    <u class="errorSearch" id="error1">
    </u>
</nav>

<?php
require_once '_footer.html';
?>
