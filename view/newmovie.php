<?php
require_once '_header.html';
require_once 'navigation.php';

if(isset($mess))echo $mess;
else echo "<br>Please make sure that the director and all actors are added first."
?>

<script type="text/javascript" src="js/newmovie.js"></script>
<br> <br>

<div id = "actor_form">
<h4>Add new actor:</h4>
<form method="post" action="cimdb.php?rt=actor/add">
  Name:
  <input type="text" name="name" id="actor_name">
  <br>
  Surname:
  <input type="text" name="surname" id="actor_surname">
  <br>
  Sex:
  <input type="text" name="sex" id="actor_sex">
  <br>
  Birth year:
  <input type="text" name="birth_y" id="actor_birth">
  <br>
  Death year:
  <input type="text" name="death_y" id="actor_death">
  <br>
  Bio:
  <br>
  <!--<input type="text" name="bio" id="actor_bio">-->
  <textarea name="bio" rows="7" cols="80" id="actor_bio"></textarea>
  <br>
  Photo:
  <br>
  <!--<input type="text" name="photo" id="actor_photo">-->
  <textarea name="photo" rows="1" cols="80" id="actor_photo"></textarea>
  <br><br>
  <input type="button" onClick="addActor(this.form);" value="Add actor">
</form>
</div>

<div id = "director_form">
<h4>Add new director:</h4>
<form method="post" action="cimdb.php?rt=director/add">
  Name:
  <input type="text" name="name" id="director_name">
  <br>
  Surname:
  <input type="text" name="surname" id="director_surname">
  <br>
  Sex:
  <input type="text" name="sex" id="director_sex">
  <br>
  Birth year:
  <input type="text" name="birth_y" id="director_birth">
  <br>
  Death year:
  <input type="text" name="death_y" id="director_death">
  <br>
  Bio:
  <br>
  <!--<input type="text" name="bio" id="director_bio">-->
  <textarea name="bio" rows="7" cols="80" id="director_bio"></textarea>
  <br>
  Photo:
  <br>
  <!--<input type="text" name="photo" id="director_photo">-->
  <textarea name="photo" rows="1" cols="80" id="director_photo"></textarea>
  <br><br>
  <input type="button" onClick="addDirector(this.form);" value="Add director">
</form>
</div>

<div id = "movie_form">
<h4>Add new movie:</h4>
<form method="post" action="cimdb.php?rt=movie/add">
  Title:
  <input type="text" name="title" id="movie_title">
  <br>
  Year:
  <input type="text" name="year" id="movie_year">
  <br>
  Genre:
  <input type="text" name="genre" id="movie_genre">
  <br>
  Description:
  <br>
  <!--<input type="text" name="description" id="movie_description">-->
  <textarea name="description" rows="7" cols="80" id="movie_description"></textarea>
  <br>
  Trailer:
  <br>
  <textarea name="link" rows="1" cols="80" id="movie_link"></textarea>
  <!--<input type="text" name="link" id="movie_link">-->
  <br>
  Photo:
  <br>
  <!--<input type="text" name="photo" id="movie_photo">-->
  <textarea name="photo" rows="1" cols="80" id="movie_photo"></textarea>
  <br>
  Director:
  <input type="text" name="director" id="movie_director">
  <br>
  <div id = "movie_actors">
  Actors:
  <br>
  <input type="text" name="actor1" id="movie_actor1"> as <input type="text" name="role1" id="movie_role1">
  <br>
  <input type="text" name="actor2" id="movie_actor2"> as <input type="text" name="role2" id="movie_role2">
  <br>
  <input type="text" name="actor3" id="movie_actor3"> as <input type="text" name="role3" id="movie_role3">
  <br>
  <input type="text" name="actor4" id="movie_actor4"> as <input type="text" name="role4" id="movie_role4">
  </div>
  <br><br>
  <input type="button" onClick="addMovie(this.form);" value="Add movie">

</form>
</div>
<?php
require_once '_footer.html';
?>
