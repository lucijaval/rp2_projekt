<?php

require_once '_header.html';
require_once 'navigation.php';

echo '<div class="titleSearchlv">'.
    '<h1 class="mainSearchTitle">Search actors</h1>'.
    '</div>';

?>

<form method="post" action="cimdb.php?rt=search/searchActors">
    <input type="text" name="nameActor" placeholder="Write name of an actor" id="txt2lv" list="list2lv">
    <input type="submit" name="search" class="searchlv" value="Search!"/>
</form>
<datalist id="list2lv"></datalist>

<nav class="otherSearchlv">
    <u class="inner" id="special2lv">MORE OPTIONS:</u>
    <a class="inner" href="cimdb.php?rt=search/mainSearch" id="alv">Return to main search!</a>
    <a class="inner" href="cimdb.php?rt=search/searchDirectors" id="alv">Search directors!</a>
    <?php
    if(isset($error)){
        echo '<u class="errorSearch" >'.$error.'</u>';
    }?>
</nav>

<?php
require_once '_footer.html';
?>
