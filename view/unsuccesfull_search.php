<?php
require_once '_header.html';
require_once 'navigation.html';

echo '<h1>Search results</h1>';

echo 'Nothing was found! Press button if you want to search again. Check spelling, and maybe try
    to put spaces between words.<br>Try writing both first name and last name.';

if($empty === "actor"){

    echo '<br><form method="post" action="cimdb.php?rt=search/searchActors">';
    echo '<input type="submit" value="Searh again"/>';
    echo '</form>';

}else{

    echo '<br><form method="post" action="cimdb.php?rt=search/searchDirectors">';
    echo '<input type="submit" value="Searh again"/>';
    echo '</form>';

}

?>
<?php

require_once '_footer.html';
?>
