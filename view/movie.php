<?php
require_once '_header.html';
require_once 'navigation.php';
?>
<script type="text/javascript" src="js/movie.js"></script>
<?php

echo '<h1>' . $movie->title . ' (' . $movie->year . ')' ;
echo '<input type="hidden" id="id_movie" name="id_movie" value="'.$movie->id.'">';
if($in_wishlist) echo ' <i class="fa fa-bookmark checked" id = "bookmark"></i></h1>';
else echo ' <i class="fa fa-bookmark notchecked" id = "bookmark"></i></h1>';
echo ' <span class="movie_score"> <span class="fa fa-star starchecked"></span> ' . $movie->score . '</span>/5 (' . sizeof($commentsList) . ' reviews)';

echo ' <span id="genre"> | ' . $movie->genre . '</span><br><br>';
echo '<img id="movie_img" src="' . $movie->photo . '" alt="' . $movie->title . '">';
echo '<iframe width="600" height="400" src="'. $movie->link .'"> </iframe> ';

echo '<div class="description"> <h3>Description: </h3>' . $movie->description . '</div>';

echo '<div class="cast"> <h3>Cast: </h3>';
foreach ($actorsList as $actor){
    echo '<a href="cimdb.php?rt=actor/show&id_actor=' . $actor->id . '">' . $actor->name . ' ' . $actor->surname . '</a>';
    echo ' as ' . $rolesList[$actor->id] . '<br>';
}
echo '</div>';

echo '<div class="director"><h3 id="h3_director">Director:</h3> <a href="cimdb.php?rt=director/show&id_director= '.$director->id.'">'. $director->name .' '. $director->surname. '</a></div>';

echo '<div class="reviews"><h3>User reviews:</h3>';
if( sizeof($commentsList) === 0 )
    echo 'No reviews.';
foreach ($commentsList as $comment){
  echo '<div id="comment"><a href="cimdb.php?rt=profile/show&id_user=' . $comment->user_id . '">' . $usernamesList[$comment->id] . '</a>';
  echo '<div id="com">'.$comment->comment.'</div><br>';
  echo '<span class="fa fa-star fa-lg starchecked"></span> <b>' . $comment->score . '</b>/5<br>';
  echo '<small>Posted: '.$comment->com_date.'</small><br></div>';


    if($_SESSION['admin_flag'] === "1") {
      echo '<form method="post" action="cimdb.php?rt=comment/delete&text=from movie&date=' . $comment->com_date . '&user_id=' . $comment->user_id . '&movie_id=' . $movie->id .'">';
      echo '<input type="button" onClick="confSubmit(this.form);" value="Delete">';
      echo '</form>';
    }
    echo '<br>';
}
echo '</div>';

echo '<div class="write_review"><h3>Review this title:</h3>';
echo '<form method="post" action="cimdb.php?rt=comment/add&id_movie=' . $movie->id . '">';?>
    <textarea name="opinion" rows="7" cols="80"></textarea>

<br>
Score:
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<span class="fa fa-star starnotchecked" id="star1"></span>
<span class="fa fa-star starnotchecked" id="star2"></span>
<span class="fa fa-star starnotchecked" id="star3"></span>
<span class="fa fa-star starnotchecked" id="star4"></span>
<span class="fa fa-star starnotchecked" id="star5"></span>

<input type="hidden" name="score" id="movie_score_input">

<input type="button" onClick="giveScore(this.form);" value="Review">
</form>
</div>
<?php
require_once '_footer.html';
?>
