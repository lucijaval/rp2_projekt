<?php

require_once '../../model/db.class.php';
/**
 * Dobavljanje konekcije
 */
$db = DB::getConnection();
/**
 * Zastavica koja pazi na to jesu li tablice već stvorene u bazi podataka
 */
$has_tables = false;
/**
 * Pripremanje tablica
 */
try
{
    $st = $db->prepare(
        'SHOW TABLES LIKE :tblname'
    );

    $st->execute( array( 'tblname' => 'project_users' ) );
    if( $st->rowCount() > 0 )
        $has_tables = true;

    $st->execute( array( 'tblname' => 'project_movies' ) );
    if( $st->rowCount() > 0 )
        $has_tables = true;

    $st->execute( array( 'tblname' => 'project_actors' ) );
    if( $st->rowCount() > 0 )
        $has_tables = true;

    $st->execute( array( 'tblname' => 'project_acting' ) );
    if( $st->rowCount() > 0 )
        $has_tables = true;

    $st->execute( array( 'tblname' => 'project_directors' ) );
    if( $st->rowCount() > 0 )
        $has_tables = true;

    $st->execute( array( 'tblname' => 'project_wishlist' ) );
    if( $st->rowCount() > 0 )
        $has_tables = true;
}
catch( PDOException $e ) { exit( "PDO error [show tables]: " . $e->getMessage() ); }

/**
 * Javljanje greške ako tablice već postoje
 */
if( $has_tables )
{
    exit( 'Tablice project_users / project_movies / project_actors / project_acting / project_directors / project_wishlist vec postoje.
    Obrisite ih pa probajte ponovno.' );
}

/**
 * Napravi tablicu project_users
 */
try
{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_users (' .
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
        'username varchar(50) NOT NULL,' .
        'password_hash varchar(255) NOT NULL,'.
        'name  varchar(20) NOT NULL,'.
        'surname varchar(20) NOT NULL,'.
        'email varchar(50) NOT NULL,' .
        'warning_flag int NOT NULL,'.
        'admin_flag int NOT NULL,'.
        'has_registered int NOT NULL,'.
        'reg_seq varchar(20) NOT NULL)'
    );

    $st->execute();
}
catch( PDOException $e ) { exit( "PDO error [create project_users]: " . $e->getMessage() ); }

echo "Napravio tablicu project_users.<br />";

/**
 * Napravi tablicu project_movies
 */
try
{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_movies (' .
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
        'title varchar(50) NOT NULL,' . //name filma
        'director_id varchar(50) NOT NULL,' .
        'year int NOT NULL,' .
        'genre varchar(50) NOT NULL,' .
        'description varchar(500) NOT NULL,' .
        'score float NOT NULL,' .
        'link varchar(150) NOT NULL,' .
        'photo varchar(150) NOT NULL)'
    );

    $st->execute();
}
catch( PDOException $e ) { exit( "PDO error [create project_movie]: " . $e->getMessage() ); }

echo "Napravio tablicu project_movies.<br />";

/**
 * Napravi tablicu project_actors
 */
try
{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_actors (' .
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
        'name varchar(20) NOT NULL,' .
        'surname varchar(20)  NOT NULL,' .
        'birth_y int NOT NULL,' .
        'death_y int,' .
        'sex varchar(1) NOT NULL,' .
        'bio varchar(800) NOT NULL,' .
        'photo varchar(150) NOT NULL)'
    );

    $st->execute();
}
catch( PDOException $e ) { exit( "PDO error [create project_actors]: " . $e->getMessage() ); }

echo "Napravio tablicu project_actors.<br />";

/**
 * Napravi tablicu project_acting
 */
try{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_acting('.
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,'.
        'actor_id int NOT NULL,'.
        'movie_id int NOT NULL,'.
        'role varchar(50) NOT NULL)'
    );
    $st->execute();

}catch(PDOException $e){exit( "PDO error [create project_acting]: " . $e->getMessage() );}

echo "Napravio tablicu project_acting.<br />";

/**
 * Napravi tablicu project_directors
 */
try{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_directors (' .
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
        'name varchar(20) NOT NULL,' .
        'surname varchar(20)  NOT NULL,' .
        'sex varchar(1) NOT NULL,'.
        'birth_y int NOT NULL,'.
        'death_y int,' .
        'bio varchar(800) NOT NULL,' .
        'photo varchar(150) NOT NULL)'
    );
    $st->execute();

}catch(PDOException $e){exit( "PDO error [create project_directors]: " . $e->getMessage() );}

echo "Napravio tablicu project_directors.<br />";

/**
 * Napravi tablicu project_wishlist
 */
try{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_wishlist('.
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,'.
        'user_id int NOT NULL,'.
        'movie_id int NOT NULL,'.
        'status int NOT NULL)'
    );
    $st->execute();

}catch(PDOException $e){exit( "PDO error [create project_wishlist]: " . $e->getMessage() );}

echo "Napravio tablicu project_wishlist.<br />";

/**
 * Napravi tablicu project_comments
 */
try{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_comments('.
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,'.
        'user_id int NOT NULL,'.
        'movie_id int NOT NULL,'.
        'comment varchar(200) NOT NULL,'.
        'score int NOT NULL,'.
        'com_date datetime NOT NULL)'
    );
    $st->execute();

}catch(PDOException $e){exit( "PDO error [create project_comments]: " . $e->getMessage() );}

echo "Napravio tablicu project_comments.<br />";


/**
 * Ubaci neke korisnike unutra
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_users(username, password_hash, name, surname, email, admin_flag, warning_flag, has_registered, reg_seq) VALUES (:username, :password, :name, :surname, \'a@b.com\', :admin_flag, \'0\', \'1\', \'abc\')' );

    /*1*/ $st->execute( array( 'username' => 'mirko', 'password' => password_hash( 'mirkovasifra', PASSWORD_DEFAULT ), 'name'=> 'Mirko', 'surname'=>'Mirkovic', 'admin_flag'=>0 ) );
    /*2*/ $st->execute( array( 'username' => 'ana', 'password' => password_hash( 'aninasifra', PASSWORD_DEFAULT ), 'name'=> 'Ana', 'surname'=>'Anovic', 'admin_flag'=>0 ) );
    /*3*/ $st->execute( array( 'username' => 'maja', 'password' => password_hash( 'majinasifra', PASSWORD_DEFAULT ), 'name'=> 'Maja', 'surname'=>'Majovic', 'admin_flag'=>0 ) );
    /*4*/ $st->execute( array( 'username' => 'slavko', 'password' => password_hash( 'slavkovasifra', PASSWORD_DEFAULT ), 'name'=> 'Slavko', 'surname'=>'Slavkovic', 'admin_flag'=>0 ) );
    /*5*/ $st->execute( array( 'username' => 'pero', 'password' => password_hash( 'perinasifra', PASSWORD_DEFAULT ), 'name'=> 'Pero', 'surname'=>'Peric', 'admin_flag'=>0 ) );
    /*6*/ $st->execute( array( 'username' => 'jelena', 'password' => password_hash( 'jeleninasifra', PASSWORD_DEFAULT ), 'name'=> 'Jelena', 'surname'=>'Kurilic', 'admin_flag'=>1 ) );
    /*7*/ $st->execute( array( 'username' => 'lucija', 'password' => password_hash( 'lucijinasifra', PASSWORD_DEFAULT ), 'name'=> 'Lucija', 'surname'=>'Valentic', 'admin_flag'=>1 ) );
    /*8*/ $st->execute( array( 'username' => 'valentina', 'password' => password_hash( 'tininasifra', PASSWORD_DEFAULT ), 'name'=> 'Valentina', 'surname'=>'Kriz', 'admin_flag'=>1 ) );
}
catch( PDOException $e ) { exit( "PDO error [insert project_users]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_users.<br />";

/**
 * Ubaci neke redatelje
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_directors(name, surname, sex, birth_y, death_y, bio, photo) VALUES (:name, :surname, :sex, :birth_y, :death_y, :bio, :photo)' );

    /*1*/ $st->execute( array( 'name' => 'Tony', 'surname' => 'Kaye', 'sex' => 'm', 'birth_y'=> 1952, 'death_y' => NULL,
                      'bio' => 'Tony Kaye is a British director of films, music videos, advertisements, and documentaries. His feature film debut was American History X (1998), a drama about racism starring Edward Norton and Edward Furlong. Kaye disowned the final cut of the film and unsuccessfully attempted to have his name removed from the credits. The film was critically lauded and Norton was nominated for the Academy Award for Best Actor for his performance in the film. The battle over artistic control of the film, which has become part of Hollywood folklore, all but destroyed Kaye\'s career. He delivered his original cut on time and within budget – but when the producer, New Line Cinema, insisted on changes, the arguments began.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY4Nzg0MTQ4Ml5BMl5BanBnXkFtZTcwMTcwMzcwNQ@@._V1_UY317_CR24,0,214,317_AL_.jpg'
                      ) );
    /*2*/ $st->execute( array( 'name' => 'Michael', 'surname' => 'Curtiz', 'sex' => 'm', 'birth_y'=> 1886, 'death_y' =>1962,
                      'bio' => 'Michael Curtiz was a Hungarian-born American film director, recognized as one of the most prolific directors in history. He directed classic films from the silent era and numerous others during Hollywood\'s Golden Age, when the studio system was prevalent. Curtiz was already a well-known director in Europe when Warner Bros. invited him to Hollywood in 1926, when he was 39 years of age. He had already directed 64 films in Europe, and soon helped Warner Bros. become the fastest-growing movie studio. He directed 102 films during his Hollywood career, mostly at Warners, where he directed ten actors to Oscar nominations.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTcyMjA2MTgwNF5BMl5BanBnXkFtZTYwMTcwMDY2._V1_UY317_CR8,0,214,317_AL_.jpg'
                      ) );
    /*3*/ $st->execute( array( 'name' => 'Alfred', 'surname' => 'Hitchcock', 'sex' => 'm', 'birth_y'=>1899 , 'death_y' => 1980,
                      'bio' => 'Sir Alfred Joseph Hitchcock KBE was an English film director and producer, widely regarded as one of the most influential filmmakers in the history of cinema. Known as "the Master of Suspense", he directed over 50 feature films in a career spanning six decades, becoming as well known as any of his actors thanks to his many interviews, his cameo roles in most of his films, and his hosting and producing of the television anthology Alfred Hitchcock Presents (1955–1965). His 53 films have grossed over US $223.3 million worldwide and garnered a total of 46 Oscar nominations and six wins. The "Hitchcockian" style includes the use of camera movement to mimic a person\'s gaze, thereby turning viewers into voyeurs, and framing shots to maximise anxiety and fear.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQxOTg3ODc2NV5BMl5BanBnXkFtZTYwNTg0NTU2._V1_UX214_CR0,0,214,317_AL_.jpg'
                      ) );
    /*4*/ $st->execute( array( 'name' => 'Jonathan', 'surname' => 'Demme', 'sex' => 'm', 'birth_y'=>1944 , 'death_y' => 2017,
                      'bio' => 'Robert Jonathan Demme was an American director, producer, and screenwriter. Throughout 1986–2004, Demme was known for his dramatic close-ups in films. This style of close-ups involves the character looking directly into the camera during crucial moments, particularly in the "Quid pro quo" scene in Silence of the Lambs. According to Demme, this was done to put the viewer into the character\'s shoes. Beginning with Rachel Getting Married (2008), Demme adopted a documentary style of filmmaking. Demme was involved in various political projects. In 1981, he directed a series of commercials for the liberal advocacy group People for the American Way.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY1NzY0OTQ0OF5BMl5BanBnXkFtZTcwNDY1Njk5Mg@@._V1_UY317_CR3,0,214,317_AL_.jpg'
                      ) );
    /*5*/ $st->execute( array( 'name' => 'Martin', 'surname' => 'Scorsese', 'sex' => 'm', 'birth_y'=>1942 , 'death_y' =>NULL,
                      'bio' => 'Martin Charles Scorsese is an American and naturalized-Italian filmmaker and historian, whose career spans more than 50 years. Scorsese\'s body of work addresses such themes as Italian-American identity (most notably Sicilian), Roman Catholic concepts of guilt and redemption, faith, machismo, modern crime, and gang conflict. Many of his films are also known for their depiction of violence and liberal use of profanity. Part of the New Hollywood wave of filmmaking, he is widely regarded as one of the most significant and influential filmmakers in cinematic history. In 1990, he founded The Film Foundation, a nonprofit organization dedicated to film preservation, and in 2007 he founded the World Cinema Foundation.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTcyNDA4Nzk3N15BMl5BanBnXkFtZTcwNDYzMjMxMw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                       ) );
    /*6*/ $st->execute( array( 'name' => 'Bryan', 'surname' => 'Singer', 'sex' => 'm', 'birth_y'=> 1965, 'death_y' => NULL,
                      'bio' => 'Bryan Jay Singer is an American director, producer and writer of film and television. He is the founder of Bad Hat Harry Productions and has produced or co-produced almost all of the films he has directed. Singer wrote and directed his first film in 1988 after graduating from a university. His film, Public Access (1993), was a co-winner of the Grand Jury Prize at the 1993 Sundance Film Festival.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTMwMzQ0OTgzNF5BMl5BanBnXkFtZTcwMjExNzAyMg@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                      ) );
    /*7*/ $st->execute( array( 'name' => 'David', 'surname' => 'Fincher', 'sex' => 'm', 'birth_y'=> 1962, 'death_y' => NULL,
                      'bio' => 'David Andrew Leo Fincher is an American film director, film producer, television director, television producer, and music video director. He was nominated for the Academy Award for Best Director for The Curious Case of Benjamin Button (2008) and The Social Network (2010). For the latter, he won the Golden Globe Award for Best Director and the BAFTA Award for Best Direction.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc1NDkwMTQ2MF5BMl5BanBnXkFtZTcwMzY0ODkyMg@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                      ) );
    /*8*/ $st->execute( array( 'name' => 'Frank', 'surname' => 'Capra', 'sex' => 'm', 'birth_y'=> 1897, 'death_y' => 1991,
                      'bio' => 'Frank Russell Capra was an Italian-American film director, producer and writer who became the creative force behind some of the major award-winning films of the 1930s and 1940s. Born in Italy and raised in Los Angeles from the age of five, his rags-to-riches story has led film historians such as Ian Freer to consider him the "American Dream personified." Capra became one of America\'s most influential directors during the 1930s, winning three Academy Awards for Best Director from six nominations, along with three other Oscar wins from nine nominations in other categories.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQ1NjE0NzgzNV5BMl5BanBnXkFtZTYwODg0MjI2._V1_UY317_CR17,0,214,317_AL_.jpg'
                      ) );
    /*9*/ $st->execute( array( 'name' => 'Steven', 'surname' => 'Spielberg', 'sex' => 'm', 'birth_y'=> 1946, 'death_y' =>NULL,
                      'bio' => 'Steven Allan Spielberg is an American filmmaker. He is considered one of the founding pioneers of the New Hollywood era and one of the most popular directors and producers in film history. Spielberg started in Hollywood directing television and several minor theatrical releases. He became a household name as the director of Jaws (1975), which was critically and commercially successful and is considered the first summer blockbuster. He co-founded Amblin Entertainment and DreamWorks Studios, where he has also served as a producer or executive producer for several successful film trilogies, tetralogies and more.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY1NjAzNzE1MV5BMl5BanBnXkFtZTYwNTk0ODc0._V1_UX214_CR0,0,214,317_AL_.jpg'
                       ) );
    /*10*/ $st->execute( array( 'name' => 'Quentin', 'surname' => 'Tarantino', 'sex' => 'm', 'birth_y'=> 1963, 'death_y' => NULL,
                      'bio' => 'Quentin Jerome Tarantino is an American filmmaker and actor. His films are characterized by nonlinear storylines, satirical subject matter, an aestheticization of violence, extended scenes of dialogue, ensemble casts consisting of established and lesser-known performers, references to popular culture and a wide variety of other films, soundtracks primarily containing songs and score pieces from the 1960s to the 1980s, and features of neo-noir film. Tarantino\'s films have garnered both critical and commercial success. He has received many industry awards, including two Academy Awards, two Golden Globe Awards, two BAFTA Awards and the Palme d\'Or, and has been nominated for an Emmy and a Grammy.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTgyMjI3ODA3Nl5BMl5BanBnXkFtZTcwNzY2MDYxOQ@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                      ) );
    /*11*/ $st->execute( array( 'name' => 'Robert', 'surname' => 'Zemeckis', 'sex' => 'm', 'birth_y'=> 1952, 'death_y' => NULL,
                      'bio' => 'Robert Lee Zemeckis is an American director, film producer and screenwriter frequently credited as an innovator in visual effects. Though Zemeckis has often been pigeonholed as a director interested only in special effects, his work has been defended by several critics including David Thomson, who wrote that "No other contemporary director has used special effects to more dramatic and narrative purpose."',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTgyMTMzMDUyNl5BMl5BanBnXkFtZTcwODA0ODMyMw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                      ) );
    /*12*/ $st->execute( array( 'name' => 'Milos', 'surname' => 'Forman', 'sex' => 'm', 'birth_y'=> 1932, 'death_y' => 2018,
                      'bio' => 'Jan Tomáš "Miloš" Forman  was a Czech-American film director, screenwriter, actor, and professor who lived and worked primarily in Czechoslovakia until 1968. Forman was an important component of the Czechoslovak New Wave. Film scholars and Czechoslovakian authorities saw his 1967 film The Firemen\'s Ball as a biting satire on Eastern European Communism, and it was banned for many years in his home country.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNDY5NDAyODM2Nl5BMl5BanBnXkFtZTcwMzgzNzg3OA@@._V1_UY317_CR13,0,214,317_AL_.jpg'
                      ) );
    /*13*/ $st->execute( array( 'name' => 'Peter', 'surname' => 'Jackson', 'sex' => 'm', 'birth_y'=> 1961, 'death_y' =>NULL,
                      'bio' => 'Sir Peter Robert Jackson ONZ KNZM is a New Zealand film director, screenwriter, and film producer. His production company is WingNut Films, and his most regular collaborators are co-writers and producers Walsh and Philippa Boyens. Jackson was made a Companion of the New Zealand Order of Merit in 2002. He was later knighted (as a Knight Companion of the New Zealand Order of Merit) by Anand Satyanand, the Governor-General of New Zealand, at a ceremony in Wellington in April 2010. In December 2014, Jackson was awarded a star on the Hollywood Walk of Fame.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY1MzQ3NjA2OV5BMl5BanBnXkFtZTcwNTExOTA5OA@@._V1_UY317_CR8,0,214,317_AL_.jpg'
                      ) );
    /*14*/ $st->execute( array( 'name' => 'Sergio', 'surname' => 'Leone', 'sex' => 'm', 'birth_y'=> 1929, 'death_y' => 1989,
                      'bio' => 'Sergio Leone was an Italian film director, producer and screenwriter, credited as the inventor of the Spaghetti Western genre and widely regarded as one of the most influential directors in the history of cinema. Leone\'s film-making style includes juxtaposing extreme close-up shots with lengthy long shots.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTk4Njk5MzY3MV5BMl5BanBnXkFtZTcwMTEyMzE0NA@@._V1_UY317_CR5,0,214,317_AL_.jpg'
                      ) );
    /*15*/ $st->execute( array( 'name' => 'Sidney', 'surname' => 'Lumet', 'sex' => 'm', 'birth_y'=> 1924, 'death_y' => 2011,
                      'bio' => 'Sidney Arthur Lumet was an American director, producer, and screenwriter with over 50 films to his credit. He was nominated five times for the Academy Award. According to The Encyclopedia of Hollywood Lumet was one of the most prolific filmmakers of the modern era, directing more than one movie a year on average since his directorial debut in 1957. Turner Classic Movies notes his "strong direction of actors", "vigorous storytelling" and the "social realism" in his best work.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY4Mzk5Mzk4Ml5BMl5BanBnXkFtZTYwMTE2NDg0._V1_UY317_CR2,0,214,317_AL_.jpg'
                      ) );
    /*16*/ $st->execute( array( 'name' => 'Francis', 'surname' => 'Ford Coppola', 'sex' => 'm', 'birth_y'=> 1939, 'death_y' => NULL,
                      'bio' => 'Francis Ford Coppola is an American film director, producer, screenwriter, film composer, and vintner. He was a central figure in the New Hollywood filmmaking movement of the 1960s and 1970s. While a number of Coppola\'s ventures in the 1980s and 1990s were critically lauded, he has never quite achieved the same commercial success with films as in the 1970s.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTM5NDU3OTgyNV5BMl5BanBnXkFtZTcwMzQxODA0NA@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                      ) );
    /*17*/ $st->execute( array( 'name' => 'Frank', 'surname' => 'Darabont', 'sex' => 'm', 'birth_y'=> 1959, 'death_y' => NULL,
                      'bio' => 'Frank Árpád Darabont is a French-born Hungarian-American film director, screenwriter and producer who has been nominated for three Academy Awards and a Golden Globe Award. In his early career, he was primarily a screenwriter for horror films such as A Nightmare on Elm Street 3: Dream Warriors, The Blob and The Fly II. As a director, he is known for his film adaptations of Stephen King novellas and novels such as The Shawshank Redemption, The Green Mile, and The Mist.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNjk0MTkxNzQwOF5BMl5BanBnXkFtZTcwODM5OTMwNA@@._V1_UY317_CR20,0,214,317_AL_.jpg'
                      ) );
    /*18*/ $st->execute( array( 'name' => 'Christopher', 'surname' => 'Nolan', 'sex' => 'm', 'birth_y'=> 1970, 'death_y' => NULL,
                      'bio' => 'Christopher Edward Nolan, CBE is an English film director, screenwriter, and producer. He is known for making distinctive, personal films within the Hollywood mainstream and is regarded as an auteur. Nolan\'s films are typically rooted in epistemological and metaphysical themes, exploring human morality, the construction of time, and the malleable nature of memory and personal identity. His body of work is permeated by materialistic perspectives, nonlinear storytelling, practical special effects, innovative soundscapes, large-format film photography, and analogous relationships between visual language and narrative elements.',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNjE3NDQyOTYyMV5BMl5BanBnXkFtZTcwODcyODU2Mw@@._V1_UY317_CR7,0,214,317_AL_.jpg'
                      ) );
  }
catch( PDOException $e ) { exit( "PDO error [project_directors]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_directors.<br />";

/**
 *  Ubaci neke filmove
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_movies(title, director_id, year, genre, description, score, link, photo) VALUES (:title, :director_id, :year, :genre, :description, :score, :link, :photo)' );

    /*1*/ $st->execute( array( 'title' => 'The Shawshank Redemption', 'director_id' => 17, 'year'=>'1994', 'genre'=>'Drama',
                      'description' => 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
                      'score' => 10, 'link' => 'https://www.youtube.com/embed/NmzuHjWmXOc',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg') );
    /*2*/ $st->execute( array( 'title' => 'The Godfather', 'director_id' => 16, 'year'=>'1972', 'genre'=>'Crime, Drama',
      	              'description' => 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/sY1S34973zA',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg') );
    /*3*/ $st->execute( array( 'title' => 'The Dark Knight', 'director_id' => 18, 'year'=>'2008', 'genre'=>'Action, Crime, Drama',
                      'description' => 'When the menace known as The Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham. The Dark Knight must accept one of the greatest psychological and physical tests of his ability to fight injustice.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/EXeTwQWrcwY',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg') );
    /*4*/ $st->execute( array( 'title' => '12 Angry Men', 'director_id' => 15, 'year'=>'1957', 'genre'=>'Drama',
                      'description' => 'A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/_13J_9B5jEk',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY1000_CR0,0,649,1000_AL_.jpg') );
    /*5*/ $st->execute( array( 'title' => 'Schindler\'s List', 'director_id' => 9, 'year'=>'1994', 'genre'=>'Drama, History',
                      'description' => 'In German-occupied Poland during World War II, industrialist Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/gG22XNhtnoY',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,666,1000_AL_.jpg') );
    /*6*/ $st->execute( array( 'title' => 'The Lord of the Rings: The Return of the King', 'director_id' => 13, 'year'=>'2003',
                      'genre'=>'Adventure, Drama, Fantasy',
                      'description' => 'Gandalf and Aragorn lead the World of Men against Sauron\'s army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring. ',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/r5X-hFf6Bwo',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg') );
    /*7*/ $st->execute( array( 'title' => 'Pulp Fiction', 'director_id' => 10, 'year'=>'1994', 'genre'=>'Crime, Drama',
                      'description' => 'The lives of two mob hitmen, a boxer, a gangster & his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/5ZAhzsi1ybM',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,686,1000_AL_.jpg') );
    /*8*/ $st->execute( array( 'title' => 'The good, the bad, and the ugly', 'director_id' => 14, 'year'=>'1966', 'genre'=>'Western',
                      'description' => 'A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.',
                      'score' => 3, 'link' => 'https://www.youtube.com/embed/WCN5JJY_wiA',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BOTQ5NDI3MTI4MF5BMl5BanBnXkFtZTgwNDQ4ODE5MDE@._V1_SY1000_CR0,0,656,1000_AL_.jpg') );
    /*9*/ $st->execute( array( 'title' => 'Fight club', 'director_id' => 7, 'year'=>'1999', 'genre'=>'Drama',
                      'description' => 'An insomniac office worker and a devil-may-care soapmaker form and underground fight club that evolves into something much, much more.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/qtRKdVHc-cE',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMjJmYTNkNmItYjYyZC00MGUxLWJhNWMtZDY4Nzc1MDAwMzU5XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,676,1000_AL_.jpg') );
    /*10*/ $st->execute( array( 'title' => 'The Lord of the Rings: The Fellowship of the Ring', 'director_id' => 13, 'year'=>'2001',
                      'genre'=>'Drama, Adventure, Fantasy',
                      'description' => 'A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/V75dMMIW2B4',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg') );
    /*11*/ $st->execute( array( 'title' => 'Forrest Gump', 'director_id' => 11, 'year'=>'1994', 'genre'=>'Drama, Romance',
                      'description' => 'The presidencies of Kennedy and Johnson, the events of Vietnam, Watergate, and other history unfold through the perspective of an Alabama man with an IQ of 75.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/XHhAG-YLdk8',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg') );
    /*12*/ $st->execute( array( 'title' => 'Inception', 'director_id' => 18, 'year'=>'2010', 'genre'=>'Action, Sci-Fi',
                      'description' => 'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/YoHD9XEInc0',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg') );
    /*13*/ $st->execute( array( 'title' => 'The Lord of the Rings: The Two Towers', 'director_id' => 13, 'year'=>'2002',
                      'genre'=>'Drama, Adventure, Fantasy',
                      'description' => 'While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron\'s new ally, Saruman, and his hordes of Isengard.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/LbfMDwc4azU',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNGE5MzIyNTAtNWFlMC00NDA2LWJiMjItMjc4Yjg1OWM5NzhhXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,684,1000_AL_.jpg') );
    /*14*/ $st->execute( array( 'title' => 'One Flew Over the Cuckoo\'s Nest', 'director_id' => 12, 'year'=>'1975', 'genre'=>'Drama',
                      'description' => 'A criminal pleads insanity after getting into trouble again and once in the mental institution rebels against the oppressive nurse and rallies up the scared patients.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/OXrcDonY-B8',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY1000_CR0,0,672,1000_AL_.jpg') );
    /*15*/ $st->execute( array( 'title' => 'Goodfellas', 'director_id' => 5, 'year'=>'1990', 'genre'=>'Crime, Drama' ,
                      'description' => 'The story of Henry Hill and his life in the mob, covering his relationship with his wife Karen Hill and his mob partners Jimmy Conway and Tommy DeVito in the Italian-American crime syndicate.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/qo5jJpHtI1Y',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX667_CR0,0,667,999_AL_.jpg') );
    /*16*/ $st->execute( array( 'title' => 'Se7en', 'director_id' => 7, 'year'=>'1995', 'genre'=>'Drama, Crime, Mystery',
                      'description' => 'Two detectives, a rookie and a veteran, hunt a serial killer who uses the seven deadly sins as his motives.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/znmZoVkCjpI',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,639,1000_AL_.jpg') );
    /*17*/ $st->execute( array( 'title' => 'The Silence of the Lambs', 'director_id' => 4, 'year'=>'1991',
                      'genre'=>'Crime, Drama, Thriller',
                      'description' => 'A young F.B.I. cadet must receive the help of an incarcerated and manipulative cannibal killer to help catch another serial killer , a madman who skins his victims.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/RuX2MQeb8UM',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNjNhZTk0ZmEtNjJhMi00YzFlLWE1MmEtYzM1M2ZmMGMwMTU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,677,1000_AL_.jpg') );
    /*18*/ $st->execute( array( 'title' => 'It\'s a Wonderful Life', 'director_id' => 8, 'year'=>'1946',
                      'genre'=>'Drama, Family, Fantasy',
                      'description' => 'An angel is sent from Heaven to help a desperately frustrated businessman by showing him what life would have been like if he had never existed.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/ewe4lg8zTYA',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BZjc4NDZhZWMtNGEzYS00ZWU2LThlM2ItNTA0YzQ0OTExMTE2XkEyXkFqcGdeQXVyNjUwMzI2NzU@._V1_SY1000_CR0,0,687,1000_AL_.jpg') );
    /*19*/ $st->execute( array( 'title' => 'Saving Private Ryan', 'director_id' => 9, 'year'=>'1998', 'genre'=>'Drama, War',
                      'description' => 'Following the Normandy Landings, a group of U.S. soldiers go behind enemy lines to retrieve a paratrooper whose brothers have been killed in action.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/RYID71hYHzg',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY1000_CR0,0,679,1000_AL_.jpg') );
    /*20*/ $st->execute( array( 'title' => 'The Usual Suspects', 'director_id' => 6, 'year'=>'1995',
                      'genre'=>'Crime, Mystery, Thriller',
                      'description' => 'A sole survivor tells of the twisty events leading up to a horrific gun battle on a boat, which began when five criminals met at a seemingly random police lineup.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/oiXdPolca5w',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BYTViNjMyNmUtNDFkNC00ZDRlLThmMDUtZDU2YWE4NGI2ZjVmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,670,1000_AL_.jpg') );
    /*21*/ $st->execute( array( 'title' => 'Interstellar', 'director_id' => 18, 'year'=>'2014', 'genre'=>'Adventure, Drama, Sci-Fi',
                      'description' => 'A team of explorers travel through a wormhole in space in an attempt to ensure humanity\'s survival.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/Lm8p5rlrSkY',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_SX675_AL_.jpg') );
    /*22*/ $st->execute( array( 'title' => 'Psycho', 'director_id' => 3, 'year'=>'1960', 'genre'=>'Horror, Mystery, Thriller',
                      'description' => 'A Phoenix secretary embezzles forty thousand dollars from her employer\'s client, goes on the run, and checks into a remote motel run by a young man under the domination of his mother.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/Wz719b9QUqY',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BNTQwNDM1YzItNDAxZC00NWY2LTk0M2UtNDIwNWI5OGUyNWUxXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,672,1000_AL_.jpg') );
    /*23*/ $st->execute( array( 'title' => 'American History X', 'director_id' => 1, 'year'=>'1998', 'genre'=>'Drama' ,
                      'description' => 'A former neo-nazi skinhead tries to prevent his younger brother from going down the same wrong path that he did.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/XfQYHqsiN5g',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BZjA0MTM4MTQtNzY5MC00NzY3LWI1ZTgtYzcxMjkyMzU4MDZiXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_.jpg') );
    /*24*/ $st->execute( array( 'title' => 'Casablanca', 'director_id' => 2, 'year'=>'1942', 'genre'=>'Drama, Romance, War',
                      'description' => 'A cynical American expatriate struggles to decide whether or not he should help his former lover and her fugitive husband escape French Morocco.',
                      'score' => 0, 'link' => 'https://www.youtube.com/embed/BkL9l7qovsE',
                      'photo' => 'https://m.media-amazon.com/images/M/MV5BY2IzZGY2YmEtYzljNS00NTM5LTgwMzUtMzM1NjQ4NGI0OTk0XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_.jpg') );
}
catch( PDOException $e ) { exit( "PDO error [project_movies]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_movies.<br />";

/**
 * Ubaci neke glumce
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_actors(name, surname, birth_y, death_y, sex, bio, photo) VALUES (:name, :surname, :birth_y, :death_y, :sex, :bio, :photo)' );

    /*1*/ $st->execute( array( 'name' => 'Jack', 'surname' => 'Nicholson', 'birth_y' => 1937, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'John Joseph Nicholson is an American actor and filmmaker who has performed for over sixty years. He is known for playing a wide range of starring or supporting roles, including satirical comedy, romance, and dark portrayals of anti-heroes and villainous characters. In many of his films, he has played the "eternal outsider, the sardonic drifter", someone who rebels against the social structure.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQ3OTY0ODk0M15BMl5BanBnXkFtZTYwNzE4Njc4._V1_.jpg') );
    /*2*/ $st->execute( array( 'name' => 'Tom', 'surname' => 'Hanks', 'birth_y' => 1956, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Thomas Jeffrey Hanks is an American actor and filmmaker. Hanks is known for his comedic and dramatic roles. Hanks\' films have grossed more than $4.6 billion at U.S. and Canadian box offices and more than $9.2 billion worldwide, making him the sixth highest-grossing actor in North America.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQ2MjMwNDA3Nl5BMl5BanBnXkFtZTcwMTA2NDY3NQ@@._V1_SY1000_CR0,0,691,1000_AL_.jpg'
                        ) );
    /*3*/ $st->execute( array( 'name' => 'Kevin', 'surname' => 'Spacey', 'birth_y' => 1959, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Kevin Spacey Fowler KBE is an American actor, producer, and singer. Spacey began his career as a stage actor during the 1980s, obtaining supporting roles in film and television. He gained critical acclaim in the 1990s that culminated in his first Academy Award for Best Supporting Actor.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY1NzMyODc3Nl5BMl5BanBnXkFtZTgwNzE2MzA1NDM@._V1_SX1223_CR0,0,1223,999_AL_.jpg'
                        ) );
    /*4*/ $st->execute( array( 'name' => 'Ian', 'surname' => 'McKellen', 'birth_y' => 1939, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Sir Ian Murray McKellen CH CBE is an English actor. His career spans genres ranging from Shakespearean and modern theatre to popular fantasy and science fiction. He is the recipient of six Laurence Olivier Awards, a Tony Award, a Golden Globe Award, a Screen Actors Guild Award, a BIF Award, two Saturn Awards, four Drama Desk Awards, and two Critics\' Choice Awards. He has also received nominations for two Academy Awards, five Primetime Emmy Awards and four BAFTAs.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQ2MjgyNjk3MV5BMl5BanBnXkFtZTcwNTA3NTY5Mg@@._V1_.jpg'
                        ) );
    /*5*/ $st->execute( array( 'name' => 'Ingrid', 'surname' => 'Bergman', 'birth_y' => 1915, 'death_y' => 1982, 'sex' => 'f',
                        'bio' => 'Ingrid Bergman was a Swedish actress who starred in a variety of European and American films. She won many accolades, including three Academy Awards, two Primetime Emmy Awards, a Tony Award, four Golden Globe Awards, and a BAFTA Award.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTYzMTgzMTIwOV5BMl5BanBnXkFtZTYwNzI5MzI2._V1_.jpg'
                        ) );
    /*6*/ $st->execute( array( 'name' => 'Edward', 'surname' => 'Norton', 'birth_y' => 1969, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Edward Harrison Norton is an American actor and filmmaker. He has received multiple awards and nominations including a Golden Globe Award and three Academy Award nominations.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTYwNjQ5MTI1NF5BMl5BanBnXkFtZTcwMzU5MTI2Mw@@._V1_.jpg'
                        ) );
    /*7*/ $st->execute( array( 'name' => 'Leonardo', 'surname' => 'DiCaprio', 'birth_y' => 1974, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Leonardo Wilhelm DiCaprio is an American actor, film producer, and environmentalist. He has been nominated for six Academy Awards, four British Academy Film Awards and nine Screen Actors Guild Awards, winning one of each award from them and three Golden Globe Awards from eleven nominations.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjI0MTg3MzI0M15BMl5BanBnXkFtZTcwMzQyODU2Mw@@._V1_.jpg') );

    /*8*/ $st->execute( array( 'name' => 'Tim', 'surname' => 'Robbins', 'birth_y' => 1958, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Timothy Francis Robbins is an American actor, screenwriter, director, producer, and musician. Robbins has written, produced, and directed several films with strong social content. Robbins released the album Tim Robbins & The Rogues Gallery Band (2010), a collection of songs written over the course of 25 years that he ultimately took on a world tour.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTI1OTYxNzAxOF5BMl5BanBnXkFtZTYwNTE5ODI4._V1_UY317_CR16,0,214,317_AL_.jpg'
                        ) );
    /*9*/ $st->execute( array( 'name' => 'Morgan', 'surname' => 'Freeman', 'birth_y' => 1937, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Morgan Freemann is an American actor, film director, and film narrator. Freeman won an Academy Award in 2005 for Best Supporting Actor, and he has received four Oscar nominations. He has also won a Golden Globe Award and a Screen Actors Guild Award. With an authoritative voice and calm demeanor, this ever popular American actor has grown into one of the most respected figures in modern US cinema.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc0MDMyMzI2OF5BMl5BanBnXkFtZTcwMzM2OTk1MQ@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*10*/ $st->execute( array( 'name' => 'Bob', 'surname' => 'Gunton', 'birth_y' => 1945, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Robert Patrick Gunton Jr. is an American actor. Gunton is known for playing strict, authoritarian characters.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc3MzY0MTQzM15BMl5BanBnXkFtZTcwMTM0ODYxNw@@._V1_UY317_CR91,0,214,317_AL_.jpg'
                        ) );
    /*11*/ $st->execute( array( 'name' => 'William', 'surname' => 'Sadler', 'birth_y' => 1950, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'William Thomas Sadler is an American film and television actor. He began his acting career in New York theaters, appearing in more than 75 productions over the course of 12 years.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTA1NjU3NDg1MTheQTJeQWpwZ15BbWU2MDI4OTcxMw@@._V1_UY317_CR25,0,214,317_AL_.jpg'
                        ) );

    /*12*/ $st->execute( array( 'name' => 'Marlon', 'surname' => 'Brando', 'birth_y' => 1924, 'death_y' => 2004, 'sex' => 'm',
                        'bio' => 'Marlon Brando Jr. was an American actor and film director. With a career spanning 60 years, he is well-regarded for his cultural influence on 20th-century film. Brando was ranked by the American Film Institute as the fourth-greatest movie star among male movie stars whose screen debuts occurred in or before 1950. He was one of six professional actors named in 1999 by Time magazine as one of its 100 Most Important People of the Century. In this list, Time also designated Brando as the "Actor of the Century".',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTg3MDYyMDE5OF5BMl5BanBnXkFtZTcwNjgyNTEzNA@@._V1_UY317_CR97,0,214,317_AL_.jpg'
                        ) );
    /*13*/ $st->execute( array( 'name' => 'Al', 'surname' => 'Pacino', 'birth_y' => 1940, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Alfredo James Pacino is an American actor and filmmaker who has had a career spanning more than five decades. He has received numerous accolades and honors both competitive and honorary, among them an Academy Award, two Tony Awards, two Primetime Emmy Awards, a British Academy Film Award, four Golden Globe Awards, the Lifetime Achievement Award from the American Film Institute, the Golden Globe Cecil B. DeMille Award and the National Medal of Arts. He is one of few performers to have won a competitive Oscar, an Emmy and a Tony Award for acting, dubbed the "Triple Crown of Acting".',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQzMzg1ODAyNl5BMl5BanBnXkFtZTYwMjAxODQ1._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*14*/ $st->execute( array( 'name' => 'James', 'surname' => 'Caan', 'birth_y' => 1940, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'James Edmund Caan is an American actor. For his contributions to the film industry, Caan was inducted into the Hollywood Walk of Fame in 1978 with a motion pictures star located at 6648 Hollywood Boulevard.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTI5NjkyNDQ3NV5BMl5BanBnXkFtZTcwNjY5NTQ0Mw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*15*/ $st->execute( array( 'name' => 'Robert', 'surname' => 'Duvall', 'birth_y' => 1931, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Robert Selden Duvall is an American actor and filmmaker whose career spans more than six decades. He has been nominated for seven Academy Awards (winning one) and seven Golden Globe Awards (winning four), and has won a BAFTA, a Screen Actors Guild Award, and an Emmy Award. He received the National Medal of Arts in 2005.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjk1MjA2Mjc2MF5BMl5BanBnXkFtZTcwOTE4MTUwMg@@._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );

    /*16*/ $st->execute( array( 'name' => 'Heath', 'surname' => 'Ledger', 'birth_y' => 1979, 'death_y' => 2008, 'sex' => 'm',
                        'bio' => 'Heath Andrew Ledger was an Australian actor and music video director. After performing roles in several Australian television and film productions during the 1990s, Ledger left for the United States in 1998 to further develop his film career. Ledger received numerous posthumous accolades for his critically acclaimed performances, including the Academy Award for Best Supporting Actor, a Best Actor International Award at the 2008 Australian Film Institute Awards, the 2008 Los Angeles Film Critics Association Award for Best Supporting Actor, the 2009 Golden Globe Award for Best Supporting Actor – Motion Picture and the 2009 BAFTA Award for Best Supporting Actor.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTI2NTY0NzA4MF5BMl5BanBnXkFtZTYwMjE1MDE0._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*17*/ $st->execute( array( 'name' => 'Christian', 'surname' => 'Bale', 'birth_y' => 1974, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Christian Charles Philip Bale is an English actor who is known for his intense method acting style, often transforming his body drastically for his roles. Bale is the recipient of many awards, including an Academy Award and two Golden Globes, and was featured in the Time 100 list of 2011.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTkxMzk4MjQ4MF5BMl5BanBnXkFtZTcwMzExODQxOA@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*18*/ $st->execute( array( 'name' => 'Aaron', 'surname' => 'Eckhart', 'birth_y' => 1968, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Aaron Edward Eckhart is an American actor. Born in Cupertino, California, Eckhart moved to England at age 13, when his father relocated the family. Several years later, he began his acting career by performing in school plays, before moving to Australia for his high school senior year. He left high school without graduating, but earned a diploma through an adult education course, and graduated from Brigham Young University (BYU) in 1994 with a Bachelor of Fine Arts degree in film. For much of the mid-1990s, he lived in New York City as a struggling, unemployed actor.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc4MTAyNzMzNF5BMl5BanBnXkFtZTcwMzQ5MzQzMg@@._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*19*/ $st->execute( array( 'name' => 'Michael', 'surname' => 'Caine', 'birth_y' => 1933, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Sir Michael Caine, CBE is an English actor, producer and author. He has appeared in more than 130 films in a career spanning 70 years and is considered a British film icon. Known for his cockney accent, Caine was born in South London. Caine is one of only two actors nominated for an Academy Award for acting in every decade from the 1960s to the 2000s. Caine appeared in seven films that featured in the British Film Institute\'s 100 greatest British films of the 20th century. In 2000, Caine received a BAFTA Fellowship, and was knighted by Queen Elizabeth II in recognition of his contribution to cinema.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjAwNzIwNTQ4Ml5BMl5BanBnXkFtZTYwMzE1MTUz._V1_UY317_CR7,0,214,317_AL_.jpg'
                        ) );

    /*20*/ $st->execute( array( 'name' => 'Martin', 'surname' => 'Balsam', 'birth_y' => 1919, 'death_y' => 1996, 'sex' => 'm',
                        'bio' => 'Martin Henry Balsam was an American character actor. His work with Hitchcock opened him up to a world of other acting opportunities. Many strong movie roles came his way in the 1960s.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQwOTE4MjMxM15BMl5BanBnXkFtZTcwMDc5MDg5Nw@@._V1_UY317_CR17,0,214,317_AL_.jpg'
                        ) );
    /*21*/ $st->execute( array( 'name' => 'John', 'surname' => 'Fiedler', 'birth_y' => 1925, 'death_y' => 2005, 'sex' => 'm',
                        'bio' => 'John Donald Fiedler was an American actor and voice actor, who was slight, balding, and bespectacled, with a distinctive, high-pitched voice. His career lasted more than 55 years in stage, film, television and radio. ',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc1Njg3NDg1OF5BMl5BanBnXkFtZTcwMjY2MTYxOA@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*22*/ $st->execute( array( 'name' => 'Lee J.', 'surname' => 'Cobb', 'birth_y' => 1911, 'death_y' => 1976, 'sex' => 'm',
                        'bio' => 'Lee J. Cobb was an American actor.  He typically played arrogant, intimidating and abrasive characters, but often had roles as respectable figures such as judges and police officers. He was twice nominated for the Academy Award for Best Supporting Actor.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BNDc3MTM0MDQyMF5BMl5BanBnXkFtZTYwMTczMTg2._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*23*/ $st->execute( array( 'name' => 'Liam', 'surname' => 'Neeson', 'birth_y' => 1952, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Liam John Neeson OBE is an actor from Northern Ireland. He has been nominated for a number of awards, including an Academy Award for Best Actor, a BAFTA Award for Best Actor in a Leading Role, and three Golden Globe Awards for Best Actor in a Motion Picture Drama. Empire magazine ranked Neeson among both the "100 Sexiest Stars in Film History" and "The Top 100 Movie Stars of All Time".',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjA1MTQ3NzU1MV5BMl5BanBnXkFtZTgwMDE3Mjg0MzE@._V1_UY317_CR52,0,214,317_AL_.jpg'
                        ) );
    /*24*/ $st->execute( array( 'name' => 'Ben', 'surname' => 'Kingsley', 'birth_y' => 1943, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Sir Ben Kingsley is an English actor with a career spanning over 50 years. He has won an Oscar, a Grammy, a BAFTA, two Golden Globes, and a Screen Actors Guild Award. Kingsley was appointed Knight Bachelor in 2002 for services to the British film industry. In 2010, he was awarded a star on the Hollywood Walk of Fame. In 2013, he received the Britannia Award for Worldwide Contribution to Filmed Entertainment.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BOTU2Njg2NzM4M15BMl5BanBnXkFtZTgwNjYwNjQwMTI@._V1_UY317_CR10,0,214,317_AL_.jpg'
                        ) );
    /*25*/ $st->execute( array( 'name' => 'Ralph', 'surname' => 'Fiennes', 'birth_y' => 1962, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Ralph Nathaniel Twisleton-Wykeham-Fiennes is an English actor, film producer, and director. A Shakespeare interpreter, he first achieved success onstage at the Royal National Theatre. Since 1999, Fiennes has served as an ambassador for UNICEF UK. Fiennes is also an Honorary Associate of London Film School. For his work behind the camera, in 2019 he received the Stanislavsky Award.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMzc5MjE1NDgyN15BMl5BanBnXkFtZTcwNzg2ODgwNA@@._V1_UY317_CR14,0,214,317_AL_.jpg'
                        ) );
    /*26*/ $st->execute( array( 'name' => 'Caroline', 'surname' => 'Goodall', 'birth_y' => 1959, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Caroline Cruice Goodall is an English actress and screenwriter. Goodall has appeared extensively on stage, joining the Royal Shakespeare Company (RSC) and then the National Theatre. As a screenwriter, in addition to The Bay of Silence, Goodall\'s credits include screen adaptations of Rupert Thomson\'s Dreams of Leaving for HKM Films.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BOTU2MDE0NzMxOV5BMl5BanBnXkFtZTcwNjE2OTg1OA@@._V1_UY317_CR8,0,214,317_AL_.jpg'
                        ) );

    /*27*/ $st->execute( array( 'name' => 'Sean', 'surname' => 'Bean', 'birth_y' => 1959, 'death_y' => NULL, 'sex' => 'm' ,
                        'bio' => 'Shaun Mark Bean, credited professionally as Sean Bean, is an English actor. After graduating from the Royal Academy of Dramatic Art, Bean made his professional debut in a theatre production of Romeo and Juliet in 1983. Retaining his Yorkshire accent, he first found mainstream success for his portrayal of Richard Sharpe in the ITV series Sharpe.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTkzMzc4MDk5OF5BMl5BanBnXkFtZTcwODg3MjUxNw@@._V1_UY317_CR8,0,214,317_AL_.jpg'
                        ) );
    /*28*/ $st->execute( array( 'name' => 'Elijah', 'surname' => 'Wood', 'birth_y' => 1981, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Elijah Jordan Wood is an American actor, film producer, and DJ. From 2011–2014, Wood played the role of Ryan Newman on the FX television series Wilfred, for which he received a Satellite Award nomination for Best Actor. Wood has his own record label, Simian Records, which he founded in 2005. In 2010, he founded the production company SpectreVision, which specializes in producing horror films.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTM0NDIxMzQ5OF5BMl5BanBnXkFtZTcwNzAyNTA4Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*29*/ $st->execute( array( 'name' => 'Orlando', 'surname' => 'Bloom', 'birth_y' => 1977, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Orlando Jonathan Blanchard Bloom is an English actor. In a 2004 poll of cultural experts conducted for the BBC, Bloom was named the twelfth-most influential person in British culture. In 2009, Bloom was named a UNICEF Goodwill Ambassador. In 2015 he received the BAFTA Britannia Humanitarian Award.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjE1MDkxMjQ3NV5BMl5BanBnXkFtZTcwMzQ3Mjc4MQ@@._V1_UY317_CR8,0,214,317_AL_.jpg'
                        ) );

    /*30*/ $st->execute( array( 'name' => 'Tim', 'surname' => 'Roth', 'birth_y' => 1961, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Timothy Simon Roth is an English actor and director. Roth made his acting début at the age of 21, playing a white supremacist skinhead named Trevor in a 1982 TV film titled Made in Britain. He received an Independent Spirit Award for Best Male Lead nomination.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjA5NTA3MDQyOV5BMl5BanBnXkFtZTcwODM4NDE3Mw@@._V1_UY317_CR16,0,214,317_AL_.jpg'
                        ) );
    /*31*/ $st->execute( array( 'name' => 'John', 'surname' => 'Travolta', 'birth_y' => 1954, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'John Joseph Travolta is an American actor, film producer, dancer, and singer. Travolta was nominated for the Academy Award for Best Actor twice. In 2010, he received the IIFA Award for Outstanding Achievement in International Cinema. In 2016, Travolta received his first Primetime Emmy Award, as a producer of the first season of the anthology series American Crime Story.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTUwNjQ0ODkxN15BMl5BanBnXkFtZTcwMDc5NjQwNw@@._V1_UY317_CR11,0,214,317_AL_.jpg'
                        ) );
    /*32*/ $st->execute( array( 'name' => 'Samuel L.', 'surname' => 'Jackson', 'birth_y' => 1948, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Samuel Leroy Jackson is an American actor and film producer. A recipient of critical acclaim and numerous accolades and awards, Jackson is the actor whose films have made the highest total gross revenue. ackson is ranked as the highest all-time box office star with over $6.7 billion total US box office gross, an average of $86.1 million per film. The worldwide box office total of his films (excluding cameo appearances) is over $15.6 billion. He became the top-grossing actor in October 2011, surpassing Frank Welker.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQ1NTQwMTYxNl5BMl5BanBnXkFtZTYwMjA1MzY1._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*33*/ $st->execute( array( 'name' => 'Bruce', 'surname' => 'Willis', 'birth_y' => 1955, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Walter Bruce Willis is an American actor, producer, and singer. His career began on the Off-Broadway stage in the 1970s. Willis is the recipient of several accolades, including a Golden Globe, two Primetime Emmy Awards, and two People\'s Choice Awards. He received a star on the Hollywood Walk of Fame in 2006.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjA0MjMzMTE5OF5BMl5BanBnXkFtZTcwMzQ2ODE3Mw@@._V1_UY317_CR27,0,214,317_AL_.jpg'
                        ) );

    /*34*/ $st->execute( array( 'name' => 'Eli', 'surname' => 'Wallach', 'birth_y' => 1915, 'death_y' => 2014, 'sex' => 'm',
                        'bio' => 'Eli Herschel Wallach was an American film, television and stage actor whose career spanned more than six decades, beginning in the late 1940s. Trained in stage acting, which he enjoyed doing most, he became "one of the greatest \'character actors\' ever to appear on stage and screen", with over 90 film credits. On stage, he often co-starred with his wife, Anne Jackson, becoming one of the best-known acting couples in the American theater. ',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjA4OTM3ODQ5MF5BMl5BanBnXkFtZTgwMzE3OTcwMjE@._V1_UY317_CR17,0,214,317_AL_.jpg'
                        ) );
    /*35*/ $st->execute( array( 'name' => 'Clint', 'surname' => 'Eastwood', 'birth_y' => 1930, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Clinton Eastwood Jr. is an American actor, filmmaker, musician, and politician. Eastwood received considerable critical praise in France for several films, including some that were not well received in the United States. Eastwood has been awarded two of France\'s highest honors: in 1994, he became a recipient of the Commander of the Ordre des Arts et des Lettres, and in 2007, he was awarded the Legion of Honour medal. In 2000, Eastwood was awarded the Italian Venice Film Festival Golden Lion for lifetime achievement. Since 1967, Eastwood\'s Malpaso Productions has produced all but four of his American films. Elected in 1986, Eastwood served for two years as mayor of Carmel-by-the-Sea, California, a non-partisan office.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTg3MDc0MjY0OV5BMl5BanBnXkFtZTcwNzU1MDAxOA@@._V1_UY317_CR10,0,214,317_AL_.jpg'
                        ) );
    /*36*/ $st->execute( array( 'name' => 'Lee', 'surname' => 'Van Cleef', 'birth_y' => 1925, 'death_y' => 1989, 'sex' => 'm',
                        'bio' => 'Clarence Leroy Van Cleef Jr. better known as Lee Van Cleef, was an American actor best known for his roles in Spaghetti Westerns. For a decade he was typecast as a minor villain, his "sinister" features overshadowing his acting skills. After suffering serious injuries in a car crash, Van Cleef had begun to lose interest in his declining career.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY0ODU1NjY5N15BMl5BanBnXkFtZTcwNTI0NzEyMw@@._V1_UY317_CR110,0,214,317_AL_.jpg'
                        ) );
    /*37*/ $st->execute( array( 'name' => 'Aldo', 'surname' => 'Giuffre', 'birth_y' => 1924, 'death_y' => 2010, 'sex' => 'm',
                        'bio' => 'Aldo Giuffre was an Italian film actor and comedian who appeared in over 90 films between 1948 and 2001. He was born in Naples and was the brother of actor Carlo Giuffre.',
                        'photo' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/AldoGiuffre.jpg/220px-AldoGiuffre.jpg'
                        ) );

    /*38*/ $st->execute( array( 'name' => 'Brad', 'surname' => 'Pitt', 'birth_y' => 1963, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'William Bradley Pitt is an American actor and film producer. He has received multiple awards and nominations including an Academy Award as producer under his own company, Plan B Entertainment. As a public figure, Pitt has been cited as one of the most influential and powerful people in the American entertainment industry. For a number of years, he was cited as the world\'s most attractive man by various media outlets, and his personal life is the subject of wide publicity.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjA1MjE2MTQ2MV5BMl5BanBnXkFtZTcwMjE5MDY0Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*39*/ $st->execute( array( 'name' => 'Helena', 'surname' => 'Bonham Carter', 'birth_y' => 1966, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Helena Bonham Carter CBE is an English actress. She is known for her roles in both low-budget independent art films and large-scale blockbusters. She was made a Commander of the Order of the British Empire (CBE) in the 2012 New Year Honours list for services to drama, and in January 2014, the British prime minister, David Cameron, announced that Bonham Carter had been appointed to Britain\'s new national Holocaust Commission.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTUzMzUzMDg5MV5BMl5BanBnXkFtZTcwMDA5NDMwNA@@._V1_UY317_CR4,0,214,317_AL_.jpg'
                        ) );
    /*40*/ $st->execute( array( 'name' => 'Zach', 'surname' => 'Grenier', 'birth_y' => 1954, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Zach Grenier is an American film, television and stage actor. He was a regular cast member on C-16 from 1997 to 1998.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BNjg3MzQxMDAxN15BMl5BanBnXkFtZTgwODMwNTk5ODE@._V1_UY317_CR27,0,214,317_AL_.jpg'
                        ) );

    /*41*/ $st->execute( array( 'name' => 'Liv', 'surname' => 'Tyler', 'birth_y' => 1977, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Liv Rundgren Tyler is an American actress and former model. yler has served as a United Nations Children\'s Fund (UNICEF) Goodwill Ambassador for the United States since 2003, and as a spokesperson for Givenchy\'s line of perfume and cosmetics. ',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY4NjQxMjc5MF5BMl5BanBnXkFtZTcwMzg5Mzg4Ng@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*42*/ $st->execute( array( 'name' => 'Viggo', 'surname' => 'Mortensen', 'birth_y' => 1958, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Viggo Peter Mortensen Jr. is an Danish-American actor, author, musician, photographer, poet, and painter. Born in New York to a Danish father and American mother, he was a resident of Venezuela and Argentina during his childhood. He is the recipient of various accolades including a Screen Actors Guild Award and has been nominated for three Academy Awards, three BAFTA Awards, and four Golden Globe Awards.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BNDQzOTg4NzA2Nl5BMl5BanBnXkFtZTcwMzkwNjkxMg@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*43*/ $st->execute( array( 'name' => 'Robin', 'surname' => 'Wright', 'birth_y' => 1966, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Robin Gayle Wright is an American actress. She is the recipient of seven Primetime Emmy Award nominations and has earned a Golden Globe Award and a Satellite Award for her work in television. Wright is also one of the highest paid actresses in the United States, earning US$420,000 per episode for her role in House of Cards in 2016.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTU0NTc4MzEyOV5BMl5BanBnXkFtZTcwODY0ODkzMQ@@._V1_UY317_CR4,0,214,317_AL_.jpg'
                        ) );
    /*44*/ $st->execute( array( 'name' => 'Gary', 'surname' => 'Sinise', 'birth_y' => 1955, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Gary Alan Sinise is an American actor, director and musician. He is known for his comedic and dramatic roles in his career. Among other awards, he has won an Emmy Award, a Golden Globe Award, a star on Hollywood Walk of Fame and has been nominated for an Academy Award. He is a supporter of various veteran organizations and founded the Lt. Dan Band who play at military bases around the world.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMzE4NzcyMzU3OV5BMl5BanBnXkFtZTYwOTM2NDE2._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*45*/ $st->execute( array( 'name' => 'Sally', 'surname' => 'Field', 'birth_y' => 1946, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Sally Margaret Field is an American actress and director. She is the recipient of various accolades, including two Academy Awards, three Primetime Emmy Awards, two Golden Globe Awards, a Screen Actors Guild Award and has been nominated for a Tony Award and two BAFTA Awards.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQwOTMyMDI4MV5BMl5BanBnXkFtZTcwMDYzMTM5OA@@._V1_UY317_CR8,0,214,317_AL_.jpg'
                        ) );

    /*46*/ $st->execute( array( 'name' => 'Joseph', 'surname' => 'Gordon-Levitt', 'birth_y' => 1981, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Joseph Leonard Gordon-Levitt is an American actor, filmmaker, singer, and entrepreneur. For his leading performances in (500) Days of Summer and 50/50, he was nominated for the Golden Globe Award for Best Actor – Motion Picture Musical or Comedy. Gordon-Levitt also founded the online production company hitRECord in 2004 and has hosted his own TV series, HitRecord on TV, since January 2014, winning the Primetime Emmy Award for Outstanding Creative Achievement in Interactive Media - Social TV Experience in the same year. In 2013, Gordon-Levitt made his feature film directing and screenwriting debut with Don Jon, in which he also stars.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BZTk5ZGQ0OGQtYWYwMy00ZTE1LWE0NWUtMTQ2MmYxMWUxZWM3XkEyXkFqcGdeQXVyMjAyNzk2Nw@@._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*47*/ $st->execute( array( 'name' => 'Ellen', 'surname' => 'Page', 'birth_y' => 1987, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Ellen Grace Philpotts-Page is a Canadian actress and producer. Her career began with roles in television shows such as Pit Pony, Trailer Park Boys and ReGenesis. Her breakthrough role was the titular character in Jason Reitman\'s film Juno (2007), for which she received nominations for an Academy Award, a BAFTA, a Golden Globe and a Screen Actors Guild Award for Best Actress and won the Independent Spirit Award, an MTV Movie Award and a Teen Choice Award.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTU3MzM3MDYzMV5BMl5BanBnXkFtZTcwNzk1Mzc3NA@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*48*/ $st->execute( array( 'name' => 'Tom', 'surname' => 'Hardy', 'birth_y' => 1977, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Edward Thomas Hardy CBE is an English actor and producer. After studying method acting at the Drama Centre London, Hardy made his film debut in Ridley Scott\'s Black Hawk Down (2001). Hardy is active in charity work and is an ambassador for the Prince\'s Trust. He was appointed Commander of the Order of the British Empire (CBE) in the 2018 Birthday Honours for services to drama.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQ3ODEyNjA4Nl5BMl5BanBnXkFtZTgwMTE4ODMyMjE@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*49*/ $st->execute( array( 'name' => 'Louise', 'surname' => 'Fletcher', 'birth_y' => 1934, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Estelle Louise Fletcher, known professionally as Louise Fletcher, is an American actress. She became only the third actress to ever win an Academy Award, BAFTA Award and Golden Globe Award for a single performance, after Audrey Hepburn and Liza Minnelli.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTIzOTcwNzYzMl5BMl5BanBnXkFtZTYwMjQ3MTI0._V1_UY317_CR7,0,214,317_AL_.jpg'
                        ) );
    /*50*/ $st->execute( array( 'name' => 'Will', 'surname' => 'Sampson', 'birth_y' => 1933, 'death_y' => 1987, 'sex' => 'm',
                        'bio' => 'William "Will" Sampson Jr. was a Native American painter, actor, and rodeo performer. Will Sampson Road, in Okmulgee County (east of Highway 75 near Preston, Oklahoma), is named for him.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTYzMzA4NjcyMl5BMl5BanBnXkFtZTcwMTAyNTUyOA@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*51*/ $st->execute( array( 'name' => 'Michael', 'surname' => 'Berryman', 'birth_y' => 1948 , 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Michael John Berryman is an American actor. Berryman was born with hypohidrotic ectodermal dysplasia, a rare condition characterised by the absence of sweat glands, hair, and fingernails; his unusual physical appearance has allowed Berryman to make a career out of portraying characters in horror movies and B movies. Berryman is a strong advocate for environmental protection and lived on a wolf sanctuary for ten years.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc0OTA0MTAwM15BMl5BanBnXkFtZTcwOTQ3MDQ4NA@@._V1_UY317_CR175,0,214,317_AL_.jpg'
                        ) );

    /*52*/ $st->execute( array( 'name' => 'Robert', 'surname' => 'De Niro', 'birth_y' => 1943, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Robert Anthony De Niro Jr. is an American actor, producer, and director. He is a recipient of numerous accolades, including two Academy Awards, a Golden Globe Award, the Cecil B DeMille Award, AFI Life Achievement Award, Presidential Medal of Freedom, and has been nominated for six BAFTA Awards, two Primetime Emmy Awards and four Screen Actors Guild Awards.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjAwNDU3MzcyOV5BMl5BanBnXkFtZTcwMjc0MTIxMw@@._V1_UY317_CR13,0,214,317_AL_.jpg'
                        ) );
    /*53*/ $st->execute( array( 'name' => 'Ray', 'surname' => 'Liotta', 'birth_y' => 1954, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Raymond Allen Liotta is an American actor, film producer, and voice actor. One of Liotta\'s earliest roles was as Joey Perrini on the soap opera Another World; he appeared on the show from 1978 to 1981. He quit the show so he could try his luck in the film industry, and moved to Los Angeles.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BZGM2OTU4NWEtNmUyMS00N2IyLWIxMmUtMDkzNGJhYWQ3MTA1XkEyXkFqcGdeQXVyMjQwMDg0Ng@@._V1_UY317_CR2,0,214,317_AL_.jpg'
                        ) );
    /*54*/ $st->execute( array( 'name' => 'Joe', 'surname' => 'Pesci', 'birth_y' =>  1943, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Joseph Frank Pesci is an American actor and singer who is known for portraying tough, volatile characters in a variety of genres and for his collaborations with Robert De Niro and Martin Scorsese. He is also known for his studio album Vincent LaGuardia Gambini Sings Just for You.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMzc3MTcxNDYxNV5BMl5BanBnXkFtZTcwOTI3NjE1Mw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*55*/ $st->execute( array( 'name' => 'Paul', 'surname' => 'Sorvino', 'birth_y' => 1939, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Paul Anthony Sorvino is an American actor, opera singer, businessman, writer, and sculptor. He often portrays authority figures on both sides of the law. He made his Broadway debut in the 1964 musical Bajour, and six years later he appeared in his first film, Carl Reiner\'s Where\'s Poppa? starring George Segal and Ruth Gordon. ',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTUyNjkyMjk1OV5BMl5BanBnXkFtZTYwNjE1NjQ1._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*56*/ $st->execute( array( 'name' => 'Gwyneth', 'surname' => 'Paltrow', 'birth_y' => 1972, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Gwyneth Kate Paltrow is an American actress, singer, author and businesswoman. She has won an Academy Award, a Golden Globe Award, a Primetime Emmy Award and two Screen Actors Guild Awards. Her films have grossed $3.2 billion at the U.S. box office and $8.2 billion worldwide. Paltrow has been the face of Estée Lauder\'s Pleasures perfume since 2005. She is also the face of American fashion brand Coach, owner of a lifestyle company, Goop, and author of several cookbooks.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BNzIxOTQ1NTU1OV5BMl5BanBnXkFtZTcwMTQ4MDY0Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*57*/ $st->execute( array( 'name' => 'Jodie', 'surname' => 'Foster', 'birth_y' => 1962, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Alicia Christian "Jodie" Foster is an American actress, director, and producer. She has received two Academy Awards, three British Academy Film Awards, two Golden Globe Awards, and the Cecil B DeMille Award. For her work as a director, she has been nominated for a Primetime Emmy Award. A child prodigy, Foster began her professional career as a child model when she was three years old, and she made her acting debut in 1968 in the television sitcom Mayberry R.F.D.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTM3MjgyOTQwNF5BMl5BanBnXkFtZTcwMDczMzEwNA@@._V1_UY317_CR1,0,214,317_AL_.jpg'
                        ) );
    /*58*/ $st->execute( array( 'name' => 'Anthony', 'surname' => 'Hopkins', 'birth_y' => 1937, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Sir Philip Anthony Hopkins CBE is a Welsh actor, director, and producer. He won the Academy Award for Best Actor in 1992, and was nominated three additional times. Hopkins has also won three BAFTAs, two Emmys, and the Cecil B. DeMille Award. In 1993, he was knighted by Queen Elizabeth II for services to the arts. Hopkins received a star on the Hollywood Walk of Fame in 2003, and in 2008, he received the BAFTA Fellowship for lifetime achievement from the British Academy of Film and Television Arts.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTg5ODk1NTc5Ml5BMl5BanBnXkFtZTYwMjAwOTI4._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*59*/ $st->execute( array( 'name' => 'Scott', 'surname' => 'Glenn', 'birth_y' => 1965, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Theodore Scott Glenn is an American actor. In 1968, he joined The Actors Studio and began working in professional theatre and TV. Some of Glenn\'s early television role was Hal Currin in the 1966 crime series Hawk.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTU3NzAwMzE1OF5BMl5BanBnXkFtZTYwMjkzOTY0._V1_UY317_CR13,0,214,317_AL_.jpg'
                        ) );
    /*60*/ $st->execute( array( 'name' => 'Anthony', 'surname' => 'Heald', 'birth_y' => 1983, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Philip Anthony Mair Heald is an American actor. Besides being a very diverse character actor, Anthony Heald has also lent his voice to audio books as well. He did readings of most of the Star Wars Expanded Universe and New Jedi Order audio books.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTEyMTQxMjcxMjdeQTJeQWpwZ15BbWU3MDk0NTY0MDg@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*61*/ $st->execute( array( 'name' => 'James', 'surname' => 'Stewart', 'birth_y' => 1908, 'death_y' => 1997, 'sex' => 'm',
                        'bio' => 'James Maitland Stewart was an American actor and military officer who is among the most honored and popular stars in film history. With a career that spanned 62 years, Stewart was a major Metro-Goldwyn-Mayer contract player who was known for his distinctive drawl and down-to-earth persona, which helped him often portray American middle-class men struggling in crisis. Many of the films in which he starred have become enduring classics.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjIwNzMzODY0NV5BMl5BanBnXkFtZTcwMDk3NDQyOA@@._V1_UY317_CR20,0,214,317_AL_.jpg'
                        ) );
    /*62*/ $st->execute( array( 'name' => 'Donna', 'surname' => 'Reed', 'birth_y' => 1921, 'death_y' => 1986, 'sex' => 'f',
                        'bio' => 'Donna Reed was an American film, television actress and producer. Her career spanned more than 40 years, with performances in more than 40 films. Reed is known for her work in television, notably as Donna Stone, a middle-class American mother and housewife in the sitcom The Donna Reed Show.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjAzNzA4MDc4NV5BMl5BanBnXkFtZTYwNTU2ODU2._V1_UY317_CR21,0,214,317_AL_.jpg'
                        ) );
    /*63*/ $st->execute( array( 'name' => 'Lionel', 'surname' => 'Barrymore', 'birth_y' => 1878, 'death_y' => 1954, 'sex' => 'm',
                        'bio' => 'Lionel Barrymore was an American actor of stage, screen and radio as well as a film director. He won an Academy Award for Best Actor for his performance in A Free Soul. He was a member of the theatrical Barrymore family.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc5NzY5MTgwNV5BMl5BanBnXkFtZTYwMjc4NjQ2._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*64*/ $st->execute( array( 'name' => 'Thomas', 'surname' => 'Mitchell', 'birth_y' => 1892, 'death_y' =>  1962, 'sex' => 'm',
                        'bio' => 'Thomas John Mitchell was an American actor. Mitchell won the Tony for Best Actor in a Musical, in 1953, for his role as Dr Downer in the musical comedy Hazel Flagg, based on the 1937 Paramount comedy film Nothing Sacred, rounding out the Triple Crown of Acting. In addition to being an actor, he was also a director, playwright, and screenwriter.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BOTcwNjAwMDI2M15BMl5BanBnXkFtZTcwNDMyNTUwOA@@._V1_UY317_CR5,0,214,317_AL_.jpg'
                        ) );

    /*65*/ $st->execute( array( 'name' => 'Tom', 'surname' => 'Sizemore', 'birth_y' => 1961, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Thomas Edward Sizemore Jr. is an American film and television actor and producer. For his performance in Heart and Souls (1993), he was nominated for the Saturn Award for Best Supporting Actor. Sizemore fronted the Hollywood rock band Day 8. ',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTUyMzQ3NDg5N15BMl5BanBnXkFtZTYwMzc0Mjk2._V1_UY317_CR13,0,214,317_AL_.jpg'
                        ) );
    /*66*/ $st->execute( array( 'name' => 'Edward', 'surname' => 'Burns', 'birth_y' => 1968, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Edward Fitzgerald Burns is an American actor, producer, writer, and director. To coincide with the 10th anniversary of the Tribeca Film Festival in 2011, Burns wrote a movie, Newlyweds that he also directed and starred in. Following a model similar to Nice Guy Johnny, Newlyweds was shot on the Canon 5D, with an even smaller crew, for $9000 in 12 days.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTY1MTU0NjIyMl5BMl5BanBnXkFtZTcwODc3OTc1MQ@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*67*/ $st->execute( array( 'name' => 'Barry', 'surname' => 'Pepper', 'birth_y' => 1970, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Barry Robert Pepper is a Canadian actor. He has been nominated for three Screen Actors Guild Awards and a Golden Globe Award. For his role as Robert F. Kennedy in the miniseries The Kennedys (2011), Pepper won the Primetime Emmy Award for Outstanding Lead Actor in a Miniseries or Movie.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTc5NTg4NjkwNF5BMl5BanBnXkFtZTcwMjIxNjc2OQ@@._V1_UY317_CR60,0,214,317_AL_.jpg'
                        ) );

    /*68*/ $st->execute( array( 'name' => 'Stephen', 'surname' => 'Baldwin', 'birth_y' => 1966, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Stephen Andrew Baldwin is an American actor, producer, and author. Baldwin began acting on television and made his film debut in The Beast. In September 2006, Baldwin released his book titled The Unusual Suspect, which details highlights from his personal life, career, days of drug abuse, and his turn to becoming a born-again Christian after the September 11 attacks in 2001.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTIyOTA4OTc5MF5BMl5BanBnXkFtZTcwOTQ3ODE5MQ@@._V1_UY317_CR131,0,214,317_AL_.jpg'
                        ) );
    /*69*/ $st->execute( array( 'name' => 'Gabriel', 'surname' => 'Byrne', 'birth_y' => 1950, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Gabriel James Byrne is an Irish actor, film director, film producer, writer, cultural ambassador and audiobook narrator. His acting career began in the Focus Theatre before he joined London\'s Royal Court Theatre in 1979. Byrne\'s screen debut came in the Irish drama serial The Riordans and the spin-off show Bracken. He has starred  in over 70 films for some of cinemas best known directors. On Broadway he received 3 Tony nominations for his roles in the work of Eugene O\'Neill as well as the Outer Critics Circle Award for A touch of the poet. On television Byrne has been nominated for 3 Emmys.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjAxMzE1ODA1Ml5BMl5BanBnXkFtZTcwODY3MjU5MQ@@._V1_UY317_CR19,0,214,317_AL_.jpg'
                        ) );
    /*70*/ $st->execute( array( 'name' => 'Benicio', 'surname' => 'Del Toro', 'birth_y' => 1967, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Benicio Monserrate Rafael del Toro Sánchez is a Puerto Rican actor. He won an Academy Award, BAFTA Award, Golden Globe Award and Screen Actors Guild Award for his portrayal of the jaded but morally upright police officer Javier Rodriguez in the film Traffic (2000).  Del Toro\'s performance as ex-con turned religious fanatic in despair, Jack Jordan, in Alejandro González Iñárritu\'s 21 Grams (2003) earned him a second Academy Award nomination for Best Supporting Actor, as well as a second Screen Actors Guild Awards nomination and a BAFTA Awards nomination for Best Actor in a Leading Role.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTkzODQ4NzU1N15BMl5BanBnXkFtZTcwOTUzMzc5Mg@@._V1_UY317_CR10,0,214,317_AL_.jpg'
                        ) );

    /*71*/ $st->execute( array( 'name' => 'Matthew', 'surname' => 'McConaughey', 'birth_y' => 1969, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Matthew McConaughey is an American actor and producer. He first gained notice for his supporting performance in the coming-of-age comedy Dazed and Confused (1993), which is considered by many to be the actor\'s breakout role. In 2013, McConaughey\'s portrayal of Ron Woodroof, a cowboy diagnosed with AIDS, in the biographical film Dallas Buyers Club earned him widespread praise and numerous accolades, including the Academy Award, Critics\' Choice Movie Award, Golden Globe Award, and Screen Actors Guild Award, all for Best Actor, among other awards and nominations. McConaughey\'s ostensible shift towards more dramatic roles since the 2000s and renewed critical appreciation has been labelled "the McConaissance".',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTg0MDc3ODUwOV5BMl5BanBnXkFtZTcwMTk2NjY4Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
    /*72*/ $st->execute( array( 'name' => 'John', 'surname' => 'Lithgow', 'birth_y' => 1945, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'John Arthur Lithgow is an American character actor, musician, poet, author, and singer. He is the recipient of numerous accolades, including two Tony Awards, six Emmy Awards, two Golden Globe Awards, three Screen Actors Guild Awards, four Drama Desk Awards, and has been nominated for two Academy Awards and four Grammy Awards. Lithgow has received a star on the Hollywood Walk of Fame and has been inducted into the American Theater Hall of Fame.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQzMzUyNDkzNF5BMl5BanBnXkFtZTcwNTMwNTU5MQ@@._V1_UY317_CR19,0,214,317_AL_.jpg'
                        ) );
    /*73*/ $st->execute( array( 'name' => 'Anne', 'surname' => 'Hathaway', 'birth_y' => 1982, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Anne Jacqueline Hathaway is an American actress and singer. One of the world\'s highest-paid actresses in 2015, she has received multiple awards, including an Academy Award, a Golden Globe, a British Academy Film Award, and a Primetime Emmy Award. Her films have earned $6.4 billion worldwide, and she appeared in the Forbes Celebrity 100 in 2009. Hathaway supports several charities. A board member of the Lollipop Theatre Network, an organization that brings films to sick children in hospitals, she advocates gender equality as a UN Women goodwill ambassador.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BNjQ5MTAxMDc5OF5BMl5BanBnXkFtZTcwOTI0OTE4OA@@._V1_UY317_CR1,0,214,317_AL_.jpg'
                        ) );

    /*74*/ $st->execute( array( 'name' => 'Anthony', 'surname' => 'Perkins', 'birth_y' => 1932, 'death_y' => 1992, 'sex' => 'm',
                        'bio' => 'Anthony Perkins was an American actor and singer. He was nominated for the Academy Award for Best Supporting Actor for his second film, Friendly Persuasion. On Broadway, he starred in the Frank Loesser musical Greenwillow (1960), for which he was nominated for another Tony Award for Best Actor in a Musical.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTIzMTUyMTYxM15BMl5BanBnXkFtZTYwNzE5OTI2._V1_UY317_CR21,0,214,317_AL_.jpg'
                        ) );
    /*75*/ $st->execute( array( 'name' => 'Vera', 'surname' => 'Miles', 'birth_y' => 1929, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Vera June Miles is a retired American actress who worked closely with Alfred Hitchcock. Miles\' first credited film appearance was in The Rose Bowl Story (1952), a romantic comedy in which she played a Tournament of Roses queen.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTgwOTY2MTk4MF5BMl5BanBnXkFtZTcwMTAwNjYwNA@@._V1_UY317_CR16,0,214,317_AL_.jpg'
                        ) );
    /*76*/ $st->execute( array( 'name' => 'John', 'surname' => 'Gavin', 'birth_y' => 1931, 'death_y' => 2018, 'sex' => 'm',
                        'bio' => 'John Gavin was an American actor who was the United States Ambassador to Mexico (1981–86) and the President of the Screen Actors Guild (1971–73). Gavin made a successful foray into live theatre in the 1970s, showcasing his baritone voice. He toured the summer stock circuit as El Gallo in a production of The Fantasticks.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTYzMDUwMDM0MV5BMl5BanBnXkFtZTYwNzE4MjM2._V1_UY317_CR22,0,214,317_AL_.jpg'
                        ) );
    /*77*/ $st->execute( array( 'name' => 'Janet', 'surname' => 'Leigh', 'birth_y' => 1927, 'death_y' => 2004, 'sex' => 'f',
                        'bio' => 'Janet Leigh was an American actress, singer, dancer, and author. Raised in Stockton, California, by working-class parents, Leigh was discovered at age eighteen by actress Norma Shearer, who helped her secure a contract with Metro-Goldwyn-Mayer. Leigh had her first formal foray into acting appearing in radio programs before making her film debut in The Romance of Rosy Ridge (1947).',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjU3MjY5OTE0MF5BMl5BanBnXkFtZTYwMzUyMDY2._V1_UY317_CR3,0,214,317_AL_.jpg'
                        ) );

    /*78*/ $st->execute( array( 'name' => 'Edward', 'surname' => 'Furlong', 'birth_y' => 1977, 'death_y' => NULL, 'sex' => 'm',
                        'bio' => 'Edward Walter Furlong is an American actor and musician. Furlong won Saturn and MTV Movie Awards for his breakthrough performance as John Connor in Terminator 2: Judgment Day (1991). The following year, he gave an Independent Spirit Award-nominated turn opposite Jeff Bridges in American Heart, and earned a second Saturn Award nomination for his work in Pet Sematary Two. He won a Young Artist Award for his performance alongside Kathy Bates in A Home of Our Own (1993).',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTI1MzgxODkyMl5BMl5BanBnXkFtZTcwNTc1NDIzMQ@@._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*79*/ $st->execute( array( 'name' => 'Beverly', 'surname' => 'D\'Angelo', 'birth_y' => 1951, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Beverly Heather D\'Angelo is an American actress and singer, who starred as Ellen Griswold in the National Lampoon\'s Vacation films (1983–2015). She has appeared in over 60 films and was nominated for a Golden Globe Award for her role as Patsy Cline in Coal Miner\'s Daughter (1980), and for an Emmy Award for her role as Stella Kowalski in the TV film A Streetcar Named Desire (1984).',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTMyNTk4ODU5NV5BMl5BanBnXkFtZTcwODU0OTgwMw@@._V1_UY317_CR6,0,214,317_AL_.jpg'
                        ) );
    /*80*/ $st->execute( array( 'name' => 'Jennifer', 'surname' => 'Lien', 'birth_y' => 1974, 'death_y' => NULL, 'sex' => 'f',
                        'bio' => 'Jennifer Anne Lien is a retired American actress, best known for playing the alien Kes on the television series Star Trek: Voyager. Her first appearance on a television series was as a music academy student in a 1990 episode of Brewster Place, starring Oprah Winfrey.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTA2MzA3OTEyNjJeQTJeQWpwZ15BbWU3MDY3NzA4OTc@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );

    /*81*/ $st->execute( array( 'name' => 'Humphrey', 'surname' => 'Bogart', 'birth_y' => 1899, 'death_y' => 1957, 'sex' => 'm',
                        'bio' => 'Humphrey DeForest Bogart was an American film and theater actor. His performances in numerous films from the Classical Hollywood era made him a cultural icon. In 1999, the American Film Institute selected him as the greatest male star of classic American cinema. Bogart began acting in Broadway shows after World War I. After the Wall Street Crash of 1929, he began his movie career in Up the River, a comedy directed by John Ford.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTIyOTE3MDM5Ml5BMl5BanBnXkFtZTYwMzA2MTM2._V1_UY317_CR14,0,214,317_AL_.jpg'
                        ) );
    /*82*/ $st->execute( array( 'name' => 'Paul', 'surname' => 'Henreid', 'birth_y' => 1908, 'death_y' => 1992, 'sex' => 'm',
                        'bio' => 'Paul Henreid was an Austrian-born American actor and film director. He trained for the theatre in Vienna, over his family\'s objections, and debuted there on the stage under the direction of Max Reinhardt. He began his film career acting in German films in the 1930s. In the early 1950s, Henreid began directing for both film and television. His television directorial credits include Alfred Hitchcock Presents, Maverick, Bonanza and The Big Valley.',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMjMyOTQ0ODg1NF5BMl5BanBnXkFtZTcwMjM2MjYwOA@@._V1_UY317_CR20,0,214,317_AL_.jpg'
                        ) );
    /*83*/ $st->execute( array( 'name' => 'Claude', 'surname' => 'Rains', 'birth_y' => 1889, 'death_y' => 1967, 'sex' => 'm',
                        'bio' => 'William Claude Rains was an English–American film and stage actor whose career spanned several decades. He was a Tony Award winning actor and was a four-time nominee for the Academy Award for Best Supporting Actor. Rains was considered to be "one of the screen\'s great character stars" who was, according to the All-Movie Guide, "at his best when playing cultured villains".',
                        'photo' => 'https://m.media-amazon.com/images/M/MV5BMTQwNDc3NjQ1OF5BMl5BanBnXkFtZTcwMjQ0NjI5Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg'
                        ) );
}
catch( PDOException $e ) { exit( "PDO error [project_actors]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_actors.<br />";

/**
 * Ubaci neke glumce u filmove
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_acting(actor_id, movie_id, role) VALUES (:actor_id, :movie_id, :role)' );
    //'The Shawshank Redemption'
    /*1*/ $st->execute( array( 'movie_id' => 1, 'actor_id' => 8, 'role' => 'Andy Dufresne') ); //Tim Robbins
    /*2*/ $st->execute( array( 'movie_id' => 1, 'actor_id' => 9, 'role' => 'Ellis Boyd Redding') ); //Morgan Freeman
    /*3*/ $st->execute( array( 'movie_id' => 1, 'actor_id' => 10, 'role' => 'Samuel Norton') ); //Bob Gunton
    /*4*/ $st->execute( array( 'movie_id' => 1, 'actor_id' => 11, 'role' => 'Heywood') ); // William Sadler
    //'The Godfather'
    /*5*/ $st->execute( array( 'movie_id' => 2, 'actor_id' => 12, 'role' => 'Vito Corleone') ); //Marlon Brando
    /*6*/ $st->execute( array( 'movie_id' => 2, 'actor_id' => 13, 'role' => 'Michael Corleone') ); //Al Pacino
    /*7*/ $st->execute( array( 'movie_id' => 2, 'actor_id' => 14, 'role' => 'Sonny Corleone') ); //James Caan
    /*8*/ $st->execute( array( 'movie_id' => 2, 'actor_id' => 15, 'role' => 'Tom Hagen') ); //Robert Duvall
    //'The Dark Knight'
    /*9*/ $st->execute( array( 'movie_id' => 3, 'actor_id' => 16, 'role' => 'Joker') ); //Heath Ledger
    /*10*/ $st->execute( array( 'movie_id' => 3, 'actor_id' => 17, 'role' => 'Bruce Wayne') ); //Christian Bale
    /*11*/ $st->execute( array( 'movie_id' => 3, 'actor_id' => 18, 'role' => 'Harvey Dent') ); //Aaron Eckhart
    /*12*/ $st->execute( array( 'movie_id' => 3, 'actor_id' => 19, 'role' => 'Alfred Pennyworth') ); //Michael Caine
    //'12 Angry Men'
    /*13*/ $st->execute( array( 'movie_id' => 4, 'actor_id' => 20, 'role' => 'Juror 1') ); //Martin Balsam
    /*14*/ $st->execute( array( 'movie_id' => 4, 'actor_id' => 21, 'role' => 'Juror 2') ); //John Fiedler
    /*15*/ $st->execute( array( 'movie_id' => 4, 'actor_id' => 22, 'role' => 'Juror 3') ); //Lee Cobb
    //'Schindler\'s List'
    /*16*/ $st->execute( array( 'movie_id' => 5, 'actor_id' => 23, 'role' => 'Oskar Schindler') ); //Liam Neeson
    /*17*/ $st->execute( array( 'movie_id' => 5, 'actor_id' => 24, 'role' => 'Itzhak Stern') ); //Ben Kingsley
    /*18*/ $st->execute( array( 'movie_id' => 5, 'actor_id' => 25, 'role' => 'Amon Goth') ); //Ralph Fiennes
    /*19*/ $st->execute( array( 'movie_id' => 5, 'actor_id' => 26, 'role' => 'Emilie Schindler') ); //Caroline Goodall
    //'The Lord of the Rings: The Return of the King'
    /*20*/ $st->execute( array( 'movie_id' => 6, 'actor_id' => 27, 'role' => 'Boromir') ); //Sean Bean
    /*21*/ $st->execute( array( 'movie_id' => 6, 'actor_id' => 28, 'role' => 'Frodo Baggins') ); //Elijah Wood
    /*22*/ $st->execute( array( 'movie_id' => 6, 'actor_id' => 29, 'role' => 'Legolas') ); //Orlando Bloom
    /*23*/ $st->execute( array( 'movie_id' => 6, 'actor_id' => 4, 'role' => 'Gandalf') ); //Ian McKellen
    //'Pulp Fiction'
    /*24*/ $st->execute( array( 'movie_id' => 7, 'actor_id' => 30, 'role' => 'Ringo') ); //Tim Roth
    /*25*/ $st->execute( array( 'movie_id' => 7, 'actor_id' => 31, 'role' => 'Vincent Vega') ); //John Travolta
    /*26*/ $st->execute( array( 'movie_id' => 7, 'actor_id' => 32, 'role' => 'Jules Winnfield') ); //Samuel L. Jackson
    /*27*/ $st->execute( array( 'movie_id' => 7, 'actor_id' => 33, 'role' => 'Butch Coolidge') ); //Bruce Willis
    //'The good, the bad, and the ugly'
    /*28*/ $st->execute( array( 'movie_id' => 8, 'actor_id' => 34, 'role' => 'Tuco Benedicto Pacifico') ); //Eli Wallach
    /*29*/ $st->execute( array( 'movie_id' => 8, 'actor_id' => 35, 'role' => 'Blondie') ); //Clint Eastwood
    /*30*/ $st->execute( array( 'movie_id' => 8, 'actor_id' => 36, 'role' => 'Angel Eyes') ); //Lee Van Cleef
    /*31*/ $st->execute( array( 'movie_id' => 8, 'actor_id' => 37, 'role' => 'Captain Clinton') ); //Aldo Giuffre
    //'Fight club'
    /*32*/ $st->execute( array( 'movie_id' => 9, 'actor_id' => 38, 'role' => 'Tyler Durden') ); //Brad Pitt
    /*33*/ $st->execute( array( 'movie_id' => 9, 'actor_id' => 39, 'role' => 'Marla Singer') ); //Helena Bonham Carter
    /*34*/ $st->execute( array( 'movie_id' => 9, 'actor_id' => 40, 'role' => 'Richard Chesler') ); //Zach Grenier
    /*35*/ $st->execute( array( 'movie_id' => 9, 'actor_id' => 6, 'role' => 'the Narator') ); //Edward Norton
    //'The Lord of the Rings: The Fellowship of the Ring'
    /*36*/ $st->execute( array( 'movie_id' => 10, 'actor_id' => 41, 'role' => 'Arwen') ); //Liv Tyler
    /*37*/ $st->execute( array( 'movie_id' => 10, 'actor_id' => 42, 'role' => 'Aragorn') ); //Viggo Mortensen
    /*38*/ $st->execute( array( 'movie_id' => 10, 'actor_id' => 28, 'role' => 'Frodo Baggins') ); //Elijah Wood
    /*39*/ $st->execute( array( 'movie_id' => 10, 'actor_id' => 29, 'role' => 'Legolas') ); //Orlando Bloom
    //'Forrest Gump'
    /*40*/ $st->execute( array( 'movie_id' => 11, 'actor_id' => 2, 'role' => 'Forrest Gump') ); //Tom Hanks
    /*41*/ $st->execute( array( 'movie_id' => 11, 'actor_id' => 43, 'role' => 'Jenny Wright') ); //Robin Wright
    /*42*/ $st->execute( array( 'movie_id' => 11, 'actor_id' => 44, 'role' => 'Dan Taylor') ); //Gary Sinise
    /*43*/ $st->execute( array( 'movie_id' => 11, 'actor_id' => 45, 'role' => 'Mrs. Gump') ); //Sally Field
    //'Inception'
    /*44*/ $st->execute( array( 'movie_id' => 12, 'actor_id' => 7, 'role' => 'Din Cobb') ); //Leonardo DiCaprio
    /*45*/ $st->execute( array( 'movie_id' => 12, 'actor_id' => 46, 'role' => 'Arthur') ); //Joseph Gordon-Levitt
    /*46*/ $st->execute( array( 'movie_id' => 12, 'actor_id' => 47, 'role' => 'Ariadne') ); //Ellen Page
    /*47*/ $st->execute( array( 'movie_id' => 12, 'actor_id' => 48, 'role' => 'Eames') ); //Tom Hardy
    //'The Lord of the Rings: The Two Towers'
    /*48*/ $st->execute( array( 'movie_id' => 13, 'actor_id' => 41, 'role' => 'Arwen') ); //Liv Tyler
    /*49*/ $st->execute( array( 'movie_id' => 13, 'actor_id' => 42, 'role' => 'Aragorn') ); //Viggo Mortensen
    /*50*/ $st->execute( array( 'movie_id' => 13, 'actor_id' => 28, 'role' => 'Frodo Baggins') ); //Elijah Wood
    /*51*/ $st->execute( array( 'movie_id' => 13, 'actor_id' => 29, 'role' => 'Legolas') ); //Orlando Bloom
    //'One Flew Over the Cuckoo\'s Nest'
    /*52*/ $st->execute( array( 'movie_id' => 14, 'actor_id' => 1, 'role' => 'Randle McMurphy') ); //Jack Nicholson
    /*53*/ $st->execute( array( 'movie_id' => 14, 'actor_id' => 49, 'role' => 'Nurse Ratched') ); //Louise Fletcher
    /*54*/ $st->execute( array( 'movie_id' => 14, 'actor_id' => 50, 'role' => 'Chief Bromden') ); //Will Sampson
    /*55*/ $st->execute( array( 'movie_id' => 14, 'actor_id' => 51, 'role' => 'Ellis') ); //Michael Berryman
    //'Goodfellas'
    /*56*/ $st->execute( array( 'movie_id' => 15, 'actor_id' => 52, 'role' => 'James Conway') ); //Robert De Niro
    /*57*/ $st->execute( array( 'movie_id' => 15, 'actor_id' => 53, 'role' => 'Henry Hill') ); //Ray Liotta
    /*58*/ $st->execute( array( 'movie_id' => 15, 'actor_id' => 54, 'role' => 'Tommy DeVito') ); //Joe Pesci
    /*59*/ $st->execute( array( 'movie_id' => 15, 'actor_id' => 55, 'role' => 'Paul Cicero') ); //Paul Sorvino
    //'Se7en'
    /*60*/ $st->execute( array( 'movie_id' => 16, 'actor_id' => 3, 'role' => 'John Doe') ); //Kevin Spacey
    /*61*/ $st->execute( array( 'movie_id' => 16, 'actor_id' => 9, 'role' => 'Detective Lieutenant William Somerset') ); //Morgan Freeman
    /*62*/ $st->execute( array( 'movie_id' => 16, 'actor_id' => 38, 'role' => 'Detective David Mills') ); //Brad Pitt
    /*63*/ $st->execute( array( 'movie_id' => 16, 'actor_id' => 56, 'role' => 'Tracy Mills') ); //Gwyneth Paltrow
    //'The Silence of the Lambs'
    /*64*/ $st->execute( array( 'movie_id' => 17, 'actor_id' => 57, 'role' => 'Clarice Starling') ); //Jodie Foster
    /*65*/ $st->execute( array( 'movie_id' => 17, 'actor_id' => 58, 'role' => 'Dr. Hannibal Lecter') ); //Anthony Hopkins
    /*66*/ $st->execute( array( 'movie_id' => 17, 'actor_id' => 59, 'role' => 'Jack Crawford') ); //Scott Glenn
    /*67*/ $st->execute( array( 'movie_id' => 17, 'actor_id' => 60, 'role' => 'Dr. Frederick Chilton') ); //Anthony Heald
    //'It\'s a Wonderful Life'
    /*68*/ $st->execute( array( 'movie_id' => 18, 'actor_id' => 61, 'role' => 'George Bailey') ); //James Stewart
    /*69*/ $st->execute( array( 'movie_id' => 18, 'actor_id' => 62, 'role' => 'Hatch Bailey') ); //Donna Reed
    /*70*/ $st->execute( array( 'movie_id' => 18, 'actor_id' => 63, 'role' => ' Mr. Henry F. Potter') ); //Lionel Barrymore
    /*71*/ $st->execute( array( 'movie_id' => 18, 'actor_id' => 64, 'role' => 'Uncle Billy Bailey') ); //Thomas Mitchell
    //'Saving Private Ryan'
    /*72*/ $st->execute( array( 'movie_id' => 19, 'actor_id' => 2, 'role' => 'Captain Miller') ); //Tom Hanks
    /*73*/ $st->execute( array( 'movie_id' => 19, 'actor_id' => 65, 'role' => 'Sergeant Horvath') ); //Tom Sizemore
    /*74*/ $st->execute( array( 'movie_id' => 19, 'actor_id' => 66, 'role' => 'Private Reiben') ); //Edward Burns
    /*75*/ $st->execute( array( 'movie_id' => 19, 'actor_id' => 67, 'role' => 'Private Jackson') ); //Barry Pepper
    //'The Usual Suspects'
    /*76*/ $st->execute( array( 'movie_id' => 20, 'actor_id' => 3, 'role' => 'Roger Kint') ); //Kevin Spacey
    /*77*/ $st->execute( array( 'movie_id' => 20, 'actor_id' => 68, 'role' => 'Private Jackson') ); //Stephen Baldwin
    /*78*/ $st->execute( array( 'movie_id' => 20, 'actor_id' => 69, 'role' => 'Dean Keaton') ); //Gabriel Byrne
    /*79*/ $st->execute( array( 'movie_id' => 20, 'actor_id' => 70, 'role' => 'Fred Fenster') ); //Benicio Del Toro
    //'Interstellar'
    /*80*/ $st->execute( array( 'movie_id' => 21, 'actor_id' => 71, 'role' => 'Cooper') ); //Matthew McConaughey
    /*81*/ $st->execute( array( 'movie_id' => 21, 'actor_id' => 72, 'role' => 'Donald') ); //John Lithgow
    /*82*/ $st->execute( array( 'movie_id' => 21, 'actor_id' => 73, 'role' => 'Amelia Brand') ); //Anne Hathaway
    /*83*/ $st->execute( array( 'movie_id' => 21, 'actor_id' => 19, 'role' => 'Professor Brand') ); //Michael Caine
    //'Psycho'
    /*84*/ $st->execute( array( 'movie_id' => 22, 'actor_id' => 74, 'role' => 'Norman Bates') ); //Anthony Perkins
    /*85*/ $st->execute( array( 'movie_id' => 22, 'actor_id' => 75, 'role' => 'Lila Crane') ); //Vera Miles
    /*86*/ $st->execute( array( 'movie_id' => 22, 'actor_id' => 76, 'role' => 'Sam Loomis') ); //John Gavin
    /*87*/ $st->execute( array( 'movie_id' => 22, 'actor_id' => 77, 'role' => 'Marion Crane') ); //Janet Leigh
    //'American History X'
    /*88*/ $st->execute( array( 'movie_id' => 23, 'actor_id' => 6, 'role' => 'Derek Vinyard') ); //Edward Norton
    /*89*/ $st->execute( array( 'movie_id' => 23, 'actor_id' => 78, 'role' => 'Danny Vinyard') ); //Edward Furlong
    /*90*/ $st->execute( array( 'movie_id' => 23, 'actor_id' => 79, 'role' => 'Doris Vinyard') ); //Beverly D'Angelo
    /*91*/ $st->execute( array( 'movie_id' => 23, 'actor_id' => 80, 'role' => 'Davina Vinyard') ); //Jennifer Lien
    //'Casablanca'
    /*92*/ $st->execute( array( 'movie_id' => 24, 'actor_id' => 5, 'role' => 'Ilsa Lund') ); //Ingrid Bergman
    /*93*/ $st->execute( array( 'movie_id' => 24, 'actor_id' => 81, 'role' => 'Rick Blaine') ); //Humphrey Bogart
    /*94*/ $st->execute( array( 'movie_id' => 24, 'actor_id' => 82, 'role' => 'Victor Laszlo') ); //Paul Henreid
    /*95*/ $st->execute( array( 'movie_id' => 24, 'actor_id' => 83, 'role' => 'Captain Louis Renault') ); //Claude Rains
}
catch( PDOException $e ) { exit( "PDO error [project_acting]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_acting.<br />";

/**
 * Ubaci neke komentare
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_comments(user_id, movie_id, comment, score, com_date) VALUES (:user_id, :movie_id, :comment, :score, :com_date)' );
    //mirko
    /*1*/ $st->execute( array( 'user_id' => 1, 'movie_id' => 1, 'comment' => 'Fantastic!', 'score'=> 5, 'com_date'=> '2019-05-28 19:11:05') ); //mirko za The Shawshank Redemption
    //ana
    /*2*/ $st->execute( array( 'user_id' => 2, 'movie_id' => 8, 'comment' => 'Ok', 'score'=> 2, 'com_date'=> '2019-05-28 15:21:11') ); //ana za The good, the bad, and the ugly

}
catch( PDOException $e ) { exit( "PDO error [project_comments]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_comments.<br />";

/**
 * Ubaci neke liste želja
 */
try
{
    $st = $db->prepare( 'INSERT INTO project_wishlist(user_id, movie_id, status) VALUES (:user_id, :movie_id, :status)' );
    //mirko
    /*1*/ $st->execute( array( 'user_id' => 1, 'movie_id' => 1, 'status' => 1) ); //mirko - The Shawshank Redemption, status 1 --> pogledao
    /*2*/ $st->execute( array( 'user_id' => 1, 'movie_id' => 24, 'status' => 0) ); //mirko - Casablanca, status 0 --> nije pogledao
    //ana
    /*3*/ $st->execute( array( 'user_id' => 2, 'movie_id' => 7, 'status' => 0) ); //ana - Pulp Fiction, status 0 --> nije pogledala
    /*4*/ $st->execute( array( 'user_id' => 2, 'movie_id' => 15, 'status' => 0) ); //ana - Goodfellas, status 0 --> nije pogledala

}
catch( PDOException $e ) { exit( "PDO error [project_wishlist]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_wishlist.<br />";

?>
