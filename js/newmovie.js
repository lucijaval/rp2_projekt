/**
 * Funckija koja pazi da je sve upisano u forumular
 * za dodavanje novog filma
 * @param form
 */
function addMovie(form) {
    if ($("#movie_title").val() == '' || $("#movie_year").val() == '' || $("#movie_director").val() == '' || $("#movie_genre").val() == ''
      || $("#movie_link").val() == '' || $("#movie_photo").val() == ''|| $("#movie_actor1").val() == '' || $("#movie_actor2").val() == ''
      || $("#movie_actor3").val() == '' || $("#movie_actor4").val() == '' || $("#movie_role1").val() == '' || $("#movie_role2").val() == ''
      || $("#movie_role3").val() == '' || $("#movie_role4").val() == '' || $("#movie_description").val() == '')
           alert("Please fill out the form completely!");

    else
      form.submit();
}

/**
 * Funckija koja pazi da je sve upisano u forumlar
 * za dodavanje novog glumca
 * @param form
 */
function addActor(form) {
    if ($("#actor_name").val() == '' || $("#actor_surname").val() == '' || $("#actor_sex").val() == '' || $("#actor_birth").val() == '' || $("#actor_bio").val() == '' || $("#actor_photo").val() == '')
              alert("Please fill out the form completely!");
    else
      form.submit();
}

/**
 * Funckija okja pazi da je sve upisano u formular
 * za dodavanje novog redatelja
 * @param form
 */
function addDirector(form) {
    if ($("#director_name").val() == '' || $("#director_surname").val() == '' || $("#director_sex").val() == '' || $("#director_birth").val() == '' || $("#director_bio").val() == '' || $("#director_photo").val() == '')
              alert("Please fill out the form completely!");
    else
      form.submit();
}
