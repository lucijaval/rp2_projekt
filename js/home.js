/**
 * Pokreće se kad je stranica otvorena
 */
$(document).ready(function () {

    $(".play-button").on("click",  play);

});
/**
 * Funkcija koja se pokreće na klik miša.
 * Ona otvara novi prozor i u njemu prikazuje trailer filma
 * @param e
 */
play = function(e){

  e.preventDefault();
  var v = $(this).attr('title');
  window.open(v, '_blank');

};
