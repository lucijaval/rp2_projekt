/**
 * Pokreće se kad je stranica otvorena
 */
$(document).ready( function()
    {
        var txt = $("#txt2lv");


        /**
         * Pokreće se kad se upiše bilo šta u
         * kućicu za pretraživanje glumaca.
         * Šalje upit za u bazu podataka, i vraćaju
         * se glumci čija imena u sebi sadrže
         * niz koji je upisan
         */
        txt.on("input", function (e) {

            var text = $(this).val();

            $.ajax(
                {
                    url: "js/grabListForSearch.php",
                    type: "GET",
                    data:{
                        search: "actor",
                        actorName: text.trim()
                    },
                    success: function(data){
                        $('#list2lv').html(data);
                    }
                }
            );
        });

    }

);
