/**
 * Pokreće se kad je stranica otvorena
 */
$(document).ready( function()
    {
        var txt = $("#txt3lv");

        /**
         * Pokreće se kad se upiše bilo šta u
         * kućicu za pretraživanje redatelja.
         * Šalje upit za u bazu podataka, i vraćaju
         * se redatelji čija imena u sebi sadrže
         * niz koji je upisan
         */
        txt.on("input", function (e) {

            var text = $(this).val();

            $.ajax(
                {
                    url: "js/grabListForSearch.php",
                    type: "GET",
                    data:{
                        search: "director",
                        directorName: text.trim()
                    },
                    success: function(data){
                        $('#list3lv').html(data);
                    }
                }
            );
        });

    }

);
