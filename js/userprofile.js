/**
 * Prikazuje se kako bi korisnik
 * potvrdio želi li obrisati komentar.
 * Ta mogućnosti je omogućena samo adminu.
 * @param form
 */
function confSubmit(form) {
  if (confirm("Are you sure you want to delete selected comment?"))
    form.submit();
  else
    alert("You decided to not delete the comment!");
}

/**
 * Pokreće se kad je stranica otvorena
 */
$(document).ready(function () {

    $('.user').each(putFlag)

});

/**
 * Funckija provjerava da li neki korisnik
 * ima 3 upozorenje, ako ima, onda se dobiva mogućnost
 * prikaza gumba za brisanje tog korisnika.
 * Ovu mogućnost ima samo admin, i isto
 * tako, korisnici ne mogu sebe sami obrisati
 */
putFlag = function(){

  var v = $(this).attr("id");
  $(this).append('</br><i class="fa fa-exclamation"></i> Warnings: '+ v);

  var id = $(this).attr("class").split(' ')[1];


  var user_id = $("#id2").html();
  var profile_id = $("#id1").html();

  html = '<div><form action="cimdb.php?rt=profile/removeuser&id_user='+id+'" method="post">&nbsp<button type="submit" style="margin:5px;"><i class="fa fa-trash-o fa-lg"></i> Delete</button></form></div>';
  if(Number(v) >= 3 && user_id != profile_id)
    $(this).append(html);
};
