/**
 * Pokreće se kad je stranica otvorena
 */
$(document).ready(function () {

    $('.warning').each(putFlag)
    $('#wish li').each(editList)

    $("#wish .close").on("click",  removeWish);

    $("#wish li").on("click", changeStatus);

    /**
     * Pokreće se kad miš pređe preko naziva
     * filma ispiše se kratki opis filma
     */
    $(document).on('mouseover', '#tooltip', function () {
      var v =  $('#desc').val();
      $(this).attr('title', v);
    });
});

/**
 * Dodaje "x" (gumb) za micanje filma iz
 * wish liste
 * @param i
 */
editList = function (i) {

  var movie = $(this).html();
  var html = '<span class="close">\u00D7</span>';
  $(this).append(html);
};

/**
 * Poziva se kad se klikne na jedan film u wish listi,
 * i mijenja se njegov status (on postaje pogledan, odnosno
 * nepogledan)
 * @param e
 */
changeStatus =  function(e){
  $(this).toggleClass("checked");

  var s = $(this).attr("class");
  var i = $(this).attr("id");
  if(s === "checked")
    s = 1;
  else {
    s = 0;
  }

  $.ajax({
        url: "cimdb.php?rt=profile/insert",
        type: "GET",
        data:
        {
            movie_id: i,
            status: s
        },
        success: function(data){
         }
      });
};
/**
 * Poziva se na klik miša, i miče
 * film iz wish lista
 */
removeWish = function(){

  var i = $(this).parent().attr("id");

  $(this).parent().remove();

  console.log(i);
  $.ajax({
        url: "cimdb.php?rt=profile/remove",
        type: "GET",
        data:
        {
            movie_id: i
        },
        success: function(data){
         }
      });
};

/**
 * Postavlja warning flag na nekog korisnika
 */
putFlag = function(){

  var v = $(this).attr("class").split(' ')[1];
  console.log(v);
  if(Number(v) >= 3)
    $(this).append('   <span class="fa fa-exclamation"></span>');
};
