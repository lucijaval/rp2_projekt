/**
 * Pokreće se kad je stranica otvorena
 */
$(document).ready( function()
    {
        var txt = $("#txt1lv");

        var sel = $("#searchBy");
        sel.on("change", function (e) {
            checkIfChosen();
        });

        txt.on("input", function (e) {

            var text = $(this).val();
            var what;
            var e = document.getElementById("searchBy");

            what = e.options[e.selectedIndex].value;

            checkIfChosen();

            if(what !== "movie"){
                return;
            }
            $.ajax(
                {
                    url: "js/grabListForSearch.php",
                    type: "GET",
                    data:{
                        search: "movie",
                        searchBar: text.trim()
                    },
                    success: function(data){
                        $('#list1lv').html(data);
                    }
                }
            );
        });

    }

);

/**
 * Funkcija koja se poziva kad god je
 * neki novi option u selectu izabran. Provjerava
 * da li je izabrana bilo koja opcija osim defaultne.
 * Ako je izabrana defaultna, onda se prikazuje greška.
 */
function checkIfChosen(){

    setTimeout(1000, this );
    var what;
    var e = document.getElementById("searchBy");

    what = e.options[e.selectedIndex].value;

    $('#error2').html("");
    $('#error1').html("");

    if($("#txt1lv").val().length === 0){
        return;
    }

    if(what === "default"){
        $('#error1').html("Choose option for searching!");
        return;
    }
}
