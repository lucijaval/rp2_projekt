<?php

require_once __DIR__ . '/../model/cimdbservice.class.php';

/**
 * Ovaj cijeli php služi kao pomoć za skriptu
 * @see searchMain.js
 */

/**
 * Dohvaća što se treba pretražiti. Može biti
 * "actor", "director" ili "movie"
 */
$search = $_GET['search'];
/**
 * Dohvaća konekciju za bazu podataka
 */
$service = new CimDbService();

if(strcmp($search ,"actor") ==0){
    $name = $_GET['actorName'];

    $response = $service->getActorStartingWith($name);

    $resp = array();

    for($i = 0;$i <  sizeof($response); $i++){
        $resp[] = $response[$i]->name. " ". $response[$i]->surname;
    }

    sendMessage($resp);

}else if(strcmp($search ,"director")==0){
    $name = $_GET['directorName'];

    $response = $service->getDirectorStartingWith($name);


    $resp = array();

    for($i = 0; $i < sizeof($response); $i++){
        $resp[] = $response[$i]->name. " ". $response[$i]->surname;
    }

    sendMessage($resp);

}else if(strcmp($search, "movie") == 0){

    $searchBar = $_GET['searchBar'];
    $response = $service->getMoviesStartingWith($searchBar);
    $resp = array();

    for($i = 0; $i < sizeof($response); $i++){

        $str = ''.$i.'';
        $resp[] = $response[$i]->title;
    }
      sendMessage($resp);
}

/**
 * Slanje poruke natrag u php
 * @param $message
 */
function sendMessage($message){
    foreach( $message as $ime )
    {echo "<option value='" . $ime . "' />\n";}
}


?>
