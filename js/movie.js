/**
 * Funckija provjerava da li je korisnik siguran
 * da želi obrisati komentar
 * @param form
 */
function confSubmit(form) {
  if (confirm("Are you sure you want to delete selected comment?"))
    form.submit();
  else
    alert("You decided not to delete the comment!");
}

/**
 * Funckija odredi koliku korisnik ocjenu
 *  želi dati, ovisno o zvjezdici na koju
 *  je stisnuo
 * @param form
 */
function giveScore(form) {
  var score=0;
  for(var i=1; i<=5; ++i){
    var string = "#star"+i;
    if($(string).hasClass( "fa fa-star starchecked" ))
      score=i;
  }
  $("#movie_score_input").val(score);
  if(score === 0) alert("You cannot give a zero!");
  else form.submit();
}

/**
 * Mijenja boju zvjezdica nakon što
 * korisnik klikne na jednu
 * @param num
 */
function changeStars(num){
  for(var i=1; i<=num; ++i){
    var string = "#star"+i;
    $(string).removeClass('fa fa-star starnotchecked');
    $(string).addClass('fa fa-star starchecked');
  }
  for(var i=num+1; i<=5; ++i){
    var string = "#star"+i;
    $(string).removeClass('fa fa-star starchecked');
    $(string).addClass('fa fa-star starnotchecked');
  }
}

/**
 * Klikom na bookmark ikonu dodaje film u wish list,
 * i mijenja boju ikone
 */
function changeBookmark(){
  console.log("da");
  if($("#bookmark").hasClass("fa fa-bookmark notchecked")){
    $("#bookmark").removeClass("fa fa-bookmark notchecked");
    $("#bookmark").addClass("fa fa-bookmark checked");

    console.log($("#id_movie").val());
    //add to wish list
    $.ajax({
          url: "cimdb.php?rt=movie/addwish",
          type: "GET",
          data:
          {
              movie_id: $("#id_movie").val(),
          },
          success: function(data){
           }
        });
  }
  else if ($("#bookmark").hasClass("fa fa-bookmark checked")){
    $("#bookmark").removeClass("fa fa-bookmark checked");
    $("#bookmark").addClass("fa fa-bookmark notchecked");

    console.log($("#id_movie").val());
    //remove from wish list
    $.ajax({
          url: "cimdb.php?rt=movie/removewish",
          type: "GET",
          data:
          {
              movie_id: $("#id_movie").val(),
          },
          success: function(data){
           }
        });
  }
}

/**
 * Prikazuje balončić koji objašnjava ikonu
 * "bookmark"
 */
function explainBookmark(){
  if($("#bookmark").hasClass("fa fa-bookmark notchecked")){
    var div = $( "<div></div>" );
    div
      .prop( "id", "balon" )
      .css(
      {
        "position": "absolute",
        "left": event.clientX,
        "top": event.clientY,
        "border": "solid 1px",
        "background-color": "rgb(245, 245, 255)",
        "padding": "5px"
      } )
      .html(
        "Click to add to wishlist."
      );
    $( "body" ).append( div );
  }
  else if ($("#bookmark").hasClass("fa fa-bookmark checked")){
    var div = $( "<div></div>" );
    div
      .prop( "id", "balon" )
      .css(
      {
        "position": "absolute",
        "left": event.clientX,
        "top": event.clientY,
        "border": "solid 1px",
        "background-color": "rgb(245, 245, 255)",
        "padding": "5px"
      } )
      .html(
        "Click to remove from wishlist."
      );
    $( "body" ).append( div );
  }
}

/**
 * Prikazuje se na početku stranice
 */
$(document).ready(function() {
  $('#star1').click(function(){
    changeStars(1);
  });
  $('#star2').click(function(){
    changeStars(2);
  });
  $('#star3').click(function(){
    changeStars(3);
  });
  $('#star4').click(function(){
    changeStars(4);
  });
  $('#star5').click(function(){
    changeStars(5);
  });

  $("#bookmark").click(function(){
    changeBookmark();
  })

  $( "#bookmark" ).on( "mouseenter", function(event){
    explainBookmark();
  } );
  $( "#bookmark" ).on( "mouseleave", function(){
    $( "#balon" ).remove();
  } );


});
