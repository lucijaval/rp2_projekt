<?php
require_once 'model/cimdbservice.class.php';

/**
 * Razred koji se poziva kad se barata sa komentarima.
 * Funckije ovog razreda nude mogućnost dodavanja i brisanja
 * komentara. Komentari se brišu/spremaju pomoću funckija
 * razreda
 * @see CimDbService
 */
class CommentController{

    /**
     * Funkcija koja se poziva kad se želi dodati neki
     * novi komentar.
     */
    public function add(){

        $service = new CimDbService();

        $user_id = $_SESSION['user_id'];
        $movie_id = $_GET['id_movie'];
        $comment = $_POST['opinion'];
        $score = $_POST['score'];
        $date = date('Y-m-d H:i:s ', time());

        $service->putComment($user_id, $movie_id, $comment, $score, $date);

        header( 'Location: cimdb.php?rt=movie/show&id_movie=' . $movie_id );
    }

    /**
     * Funckija koja se poziva kad se neki komentar želi obrisati
     */
    public function delete(){

        $service = new CimDbService();

        $date = $_GET['date'];
        $user_id = $_GET['user_id'];
        $movie_id = $_GET['movie_id'];

        $service->deleteComment($user_id, $date, $movie_id);
        $service->warnUser($user_id);

        if($_GET['text'] === "from movie")
          header( 'Location: cimdb.php?rt=movie/show&id_movie=' . $movie_id );
        else header( 'Location: cimdb.php?rt=profile/show&id_user=' . $user_id );
    }

};


?>
