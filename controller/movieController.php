<?php

require_once __DIR__ . '/../model/cimdbservice.class.php';
/**
 * URL je oblika cimdb.php?rt=movie/show&id_movie=1 --> za view/movie.php
 * fj-a index daje popis top-rated filmova
 */

/**
 * Razred movieController čije metode su zadužene
 * za prikaz ili dodavanje filmova u bazu podataka. Služi se
 * funckijama drugog razreda, @see CimDbService
 */
class movieController
{
    /**
     * Funkcija koja priprema top 10 filmova
     * za prikaz na stranici @see movie_list.php
     */
    public function index()
    {
        $service = new CimDbService();

        $moviesList = $service->getTopTenMovies();
        $wishList = $service->getWishlist($_SESSION['user_id']);
        $in_wishlist = array();

        foreach ($moviesList as $m) {
            $in_wishlist[$m->id] = in_array($m, $wishList);
        }

        $topTen = "nijeSearch";

        require_once __DIR__ . '/../view/movie_list.php';
    }

    /**
     * Funckija koja priprema podatke za prikaz nekog filma
     */
    public function show()
    {
        $ser = new CimDbService();
        $movie = $ser->getMovieById($_GET['id_movie']);
        $commentsList = $ser->getMovieComments($_GET['id_movie']);
        $actorsList = $ser->getActorsByMovie($_GET['id_movie']);
        $director = $ser->getDirectorByMovie($_GET['id_movie']);
        $wishList = $ser->getWishlist($_SESSION['user_id']);
        $in_wishlist = in_array($movie, $wishList);
        $rolesList = array();
        $usernamesList = array();
        $useridsList = array();
        foreach ($actorsList as $actor){
            $role = $ser->getRole($actor->id, $_GET['id_movie']);
            $rolesList[$actor->id] = $role;
        }

        foreach ($commentsList as $comment){
            $user = $ser->getUserById($comment->user_id);
            $username = $user->username;

            $usernamesList[$comment->id] = $username;
            $useridsList[$comment->id] = $comment->user_id;
        }

        require_once __DIR__ . '/../view/movie.php';
    }

    /**
     * Funckija koja se poziva kad se želi dodati neki novi film.
     * Šalje korisnika na stranicu koja prikazuje forumlar
     * za dodavanje filmova, redatelja i glumca
     */
    public function newMovie()
    {
      require_once __DIR__ . '/../view/newmovie.php';
    }

    /**
     * Funckija koja se poziva kad se želi dodati film
     * u wish list
     */
    public function addwish(){

      $service = new CimDbService();
      $service->putInWishList($_SESSION['user_id'], $_GET['movie_id']);

    }

    /**
     * Funckija koja se poziva kad se želi maknuti
     * film iz wish lista
     */
    public function removewish(){

      $service = new CimDbService();
      $service->removeWish($_SESSION['user_id'], $_GET['movie_id']);

    }

    /**
     * Funckija koja se poziva kad se želi dodati neki
     * novi film u bazu podataka
     */
    public function add()
    {
      $ser = new CimDbService();
      $title = $_POST['title'];
      $year = $_POST['year'];
      $genre = $_POST['genre'];
      $photo = $_POST['photo'];
      $link = $_POST['link'];
      $directorname = $_POST['director'];
      $description = $_POST['description'];

      $actorsList = array();
      for($i=0; $i<4; ++$i){
        $name = $_POST['actor'.($i+1)];
        $arr = explode(" ", $name);
        $name = strtolower($arr[0]);
        $name = ucfirst($name);
        $surname = strtolower($arr[1]);
        $surname = ucfirst($surname);
        $actor = $ser->getActorByName(trim($name), trim($surname));
        if(empty($actor)){
          $actor = 'Actor missing';
        }
        $actorsList[] = $actor;
      }

      $arr = explode(" ", $directorname);
      $name = strtolower($arr[0]);
      $name = ucfirst($name);
      $surname = strtolower($arr[1]);
      $surname = ucfirst($surname);
      $director = $ser->getDirectorByName(trim($name), trim($surname));
      if(empty($director)){
        $director = 'Director missing';
      }

      $rolesList = array();
      for($i = 0; $i < 4; ++$i){
        $actor = $actorsList[$i];
        $role = $_POST['role'. ($i+1)];
        $rolesList[$actor->id] = $role;
      }

      $mess = '';
      if($director === 'Director missing' ){
        $mess = 'Please add the director first.';
        require_once __DIR__ . '/../view/newmovie.php';
      }
      else if(in_array("Actor missing", $actorsList) ){
        $mess = 'Please add all actors first.';
        require_once __DIR__ . '/../view/newmovie.php';
      }
      else{
        $movie = $ser->addMovie($title, $year, $genre, $photo, $link, $actorsList, $rolesList, $director, $description);
        $commentsList = $ser->getMovieComments($movie->id);
        foreach ($commentsList as $comment){
            $user = $ser->getUserById($comment->user_id);
            $username = $user->username;

            $usernamesList[$comment->id] = $username;
            $useridsList[$comment->id] = $comment->user_id;
        }
        $in_wishlist = false;
        require_once __DIR__ . '/../view/movie.php';
      }
    }
};
?>
