<?php

require_once __DIR__ . '/../model/cimdbservice.class.php';

/**
 * Razred searchController koji nudi funckije koje
 * služe kod pretraživanja filmova, glumaca i reatelja.
 * Koristi funckije iz @see CimDbService razreda
 * za pretraživanje baze podataka
 */
class searchController
{
    /**
     * Prikazuje glavnu stranicu za pretraživanje
     */
    public function index()
    {
        require_once __DIR__ . '/../view/searchMain.php';
    }

    /**
     * Prikazuje stranicu za pretraživanje glumaca,
     * i ako se neki glumac traži, onda se provjerava
     * string koji je upisan. Ako je glumac nađen, prikazuje
     * se njegova stranica, inače se javlja greška
     */
    public function searchActors(){

        if(!isset($_POST['search'])){
            require_once 'view/searchActors.php';
            return;
        }


        if(empty($_POST['nameActor'])){
            $error = "Invalid. You haven't written anything!";
            require_once 'view/searchActors.php';
            return;
        }

        $service = new CimDbService();
        $name = $_POST['nameActor'];

        $arr = explode(" ", $name);

        if(sizeof($arr) < 2){
            $error = "Invalid. You have to write name AND last name!";
            require_once 'view/searchActors.php';
            return;
        }

        $name = strtolower($arr[0]);
        $name = ucfirst($name);
        $surname = strtolower($arr[1]);
        $surname = ucfirst($surname);

        $actor = $service->getActorByName(trim($name), trim($surname));

        if(empty($actor)){
            $error = 'actor';
            require_once 'view/searchActors.php';
            return;

        }

        $moviesList = $service->getMoviesByActor($actor->id);
        $rolesList = array();

        foreach ($moviesList as $movie){
            $role = $service->getRole($actor->id, $movie->id);
            $rolesList[$movie->id] = $role;
        }

        require_once 'view/actor.php';
    }

    /**
     * Prikazuje stranicu za pretraživanje redatelja,
     * i ako se neki redatelj traži, onda se provjerava
     * string koji je upisan. Ako je redatelj nađen, prikazuje
     * se njegova stranica, inače se javlja greška
     */
    public function searchDirectors(){

        if(!isset($_POST['search'])){
            require_once 'view/searchDirectors.php';
            return;
        }

        if(empty($_POST['nameDirector'])){
            $error = "Invalid. You haven't written anything!";
            require_once 'view/searchDirectors.php';
            return;
        }

        $service = new CimDbService();
        $name = $_POST['nameDirector'];

        $arr = explode(" ", $name);


        if(sizeof($arr) < 2){
            $error = "Invalid. You have to write name AND last name!";
            require_once 'view/searchDirectors.php';
            return;
        }


        $name = strtolower($arr[0]);
        $name = ucfirst($name);
        $surname = strtolower($arr[1]);
        $surname = ucfirst($surname);

        $director = $service->getDirectorByName(trim($name), trim($surname));

        if(empty($director)){
            $error = 'director';
            require_once 'view/searchDirectors.php';
            return;
        }


        $moviesList = $service->getMoviesByDirector($director->id);

        require_once 'view/director.php';


    }

    /**
     * Funckija koja provjerava unesene podatke za pretraživanje.
     * Ako su neki filmovi pronađeni, onda se prikazuje stranica
     * sa popisom tih filmova, inače se javlja greška i korisnik je
     * vraćen na stranicu za pretraživanje
     */
    public function search()
    {
        $byWhat = $_POST['searchBy'];
        $name = $_POST['nameThing'];
        $service = new CimDbService();


       // echo $byWhat;
        if(empty($_POST['nameThing'])&&isset($_POST['searchBy']) && $_POST['searchBy']==="default"){

            $error = "Invalid. Write something, and then choose by what you want to search!";
            require_once 'view/searchMain.php';
            return;
        }


        if(isset($_POST['searchBy']) && $_POST['searchBy']==="default"){

            $error = "Invalid. You haven't choose option!";
            require_once 'view/searchMain.php';
            return;
        }

        if(empty($_POST['nameThing'])){
            $error = "Invalid. You haven't written anything!";
            require_once 'view/searchMain.php';
            return;
        }

        switch($byWhat){

            case "year":

                $moviesList = $service->getMoviesByYear(trim($name));

                if(empty($moviesList) || sizeof($moviesList) === 0){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;
                }
                $wishList = $service->getWishlist($_SESSION['user_id']);
                $in_wishlist = array();

                foreach ($moviesList as $m) {
                    $in_wishlist[$m->id] = in_array($m, $wishList);
                }
                require_once 'view/movie_list.php';
                break;

            case "movie":

                $names = explode(" ", $name);

                $arr = array();
                foreach($names as $n){
                    $n = strtolower($n);
                    $n = ucfirst($n);
                    $arr[] = $n;
                }

                $name = implode(" ", $arr);

                $moviesList = $service->getMoviesByName(trim($name));
                $wishList = $service->getWishlist($_SESSION['user_id']);
                $in_wishlist = array();

                foreach ($moviesList as $m) {
                    $in_wishlist[$m->id] = in_array($m, $wishList);
                }
                if(empty($moviesList)){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;
                }
                require_once 'view/movie_list.php';

                break;
            case "director":

                $arr = explode(" ", $name);

                if(sizeof($arr) < 2){
                    $error = "Invalid. You have to write name AND last name!";
                    require_once 'view/searchMain.php';
                    return;
                }


                $name = strtolower($arr[0]);
                $name = ucfirst($name);
                $surname = strtolower($arr[1]);
                $surname = ucfirst($surname);

                $director = $service->getDirectorByName(trim($name), trim($surname));

                if(empty($director)){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;

                }

                $moviesList = $service->getMoviesByDirector($director->id);

                $wishList = $service->getWishlist($_SESSION['user_id']);
                $in_wishlist = array();

                foreach ($moviesList as $m) {
                    $in_wishlist[$m->id] = in_array($m, $wishList);
                }
                if(empty($moviesList) || sizeof($moviesList) === 0){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;
                }
                require_once 'view/movie_list.php';

                break;
            case "genre":

                $name = strtolower($name);
                $name = ucfirst($name);
                $name = trim($name);
                $moviesList = $service->getMovieByGenre($name);

                $wishList = $service->getWishlist($_SESSION['user_id']);
                $in_wishlist = array();

                foreach ($moviesList as $m) {
                    $in_wishlist[$m->id] = in_array($m, $wishList);
                }
                if(empty($moviesList) || sizeof($moviesList) === 0){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;
                }
                require_once 'view/movie_list.php';

                break;
            case "actor":

                $arr = explode(" ", $name);


                if(sizeof($arr) < 2){
                    $error = "Invalid. You have to write name AND last name!";
                    require_once 'view/searchMain.php';
                    return;
                }

                $name = strtolower($arr[0]);
                $name = ucfirst($name);
                $surname = strtolower($arr[1]);
                $surname = ucfirst($surname);

                $actor = $service->getActorByName(trim($name), trim($surname));

                if(empty($actor)){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;

                }

                $moviesList = $service->getMoviesByActor($actor->id);

                if(empty($moviesList)){
                    $error = 'Not found';
                    require_once 'view/searchMain.php';
                    return;
                }

                $wishList = $service->getWishlist($_SESSION['user_id']);
                $in_wishlist = array();

                foreach ($moviesList as $m) {
                    $in_wishlist[$m->id] = in_array($m, $wishList);
                }
                require_once 'view/movie_list.php';

                break;

        }
    }
};
?>
