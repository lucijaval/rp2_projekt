<?php

require_once 'model/cimdbservice.class.php';

/**
 * Razred directorController koji iz baze, pomoću funkcija iz razreda,
 * dohvaća informacije o redatelju i prikazuje profil redatelja. Isto
 * tako, nudi funckiju koja ubacuje novog redatelja u bazu.
 *
 * @see CimDbService
 */
class DirectorController{

    /**
     * Prikazuje profil redatelja
     */
    public function show(){
        $service = new CimDbService();

        $director = $service->getDirectorById($_GET['id_director']);

        $moviesList = $service->getMoviesByDirector($director->id);

        require_once 'view/director.php';
    }

    /**
     * Funckija se poziva kad se želi dodati neki
     * novi redalje u bazu podataka
     */
    public function add(){
        $service = new CimDbService();

        $director = $service->addDirector($_POST['name'], $_POST['surname'], $_POST['sex'], $_POST['birth_y'], $_POST['death_y'], $_POST['bio'], $_POST['photo']);
        $moviesList = $service->getMoviesByDirector($director->id);

        require_once 'view/director.php';
    }
};

?>
