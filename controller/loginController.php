<?php
require_once 'model/cimdbservice.class.php';

/**
 * Razred loginController koji šalje korisnika
 * na stranicu/prikaz formulara za login, ali isto tako provjerava
 * podatke koje je korisnik upisao, te mu ili javlja grešku,
 * ili šalje korisnika na homepage
 */
class loginController
{
    /**
     * Glavna funckija,
     * provjerava unesene podatke, ali isto tako
     * šalje korisnika na stranicu sa formularom
     * za login
     */
    public function index()
    {

        if(isset($_SESSION['user_id']) && isset($_SESSION['username'])){
            unset($_SESSION['user_id']);
            unset($_SESSION['username']);
        }

        if( isset( $_GET['mess'] ) ) echo '<div id = "loginerror">' . $_GET['mess'] . '</div>';
        if(!isset($_POST['login'])) require_once 'view/login.php';
        else{

            if((isset($_POST['login']) && isset($_POST['password']) && isset($_POST['username']))) {
                $ser = new CimDbService();

                $array = $ser->check_login($_POST['username'], $_POST['password']);

                if($array[0] != false && $array[1] != false) {
                    $_SESSION['user_id'] = $array[0];
                    $_SESSION['username'] = $array[1];
                    $user = $ser->getUserById($array[0]);
                    $_SESSION['admin_flag'] = $user->admin_flag;
                    header( 'Location: cimdb.php?rt=home' );
                }
                else if($array[0] === true && $array[1] === false)
                    header( 'Location: cimdb.php?rt=login&mess=You can log in after you confirm your registration!' );
                else header( 'Location: cimdb.php?rt=login&mess=Incorrect username or password!' );
            }
        }
    }
};

?>
