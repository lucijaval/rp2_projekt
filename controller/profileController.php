<?php

require_once __DIR__ . '/../model/cimdbservice.class.php';
/**
 * URL je oblika cimdb.php?rt=profile/show&id_user=1 za druge korisnike
 *               cimdb.php?rt=profile/myprofile za korisnika koji je trenutno logiran
 */

/**
 * Razred profileController koji se poziva kad se prikazuje
 * profil nekog korisnika. Nudi funckije koje dobavljaju podatke
 * o korisniku/korisnicima koristeći funckije
 * razreda @see CimDbService
 */
class profileController
{
    /**
     * Funckija koja priprema podatke za prikaz profila nekog korisnika.
     * Profil admina i obučnog korisnika se razlikuje
     */
    public function myprofile()
    {
        $ser = new CimDbService();
        $user = $ser->getUserById($_SESSION['user_id']);
        $commentList = $ser->getMyComments($_SESSION['user_id']);

        $movie = array();
        foreach($commentList as $comment){
            $movie[] = $ser->getMovieById($comment->movie_id);
        }

        $wishList = $ser->getWishlist($_SESSION['user_id']);
        $wish = $ser->getWishes($_SESSION['user_id']);
        $users = $ser->getAllUser($_SESSION['user_id']);

        require_once __DIR__ . '/../view/myprofile.php';
    }

    /**
     * Funckija koja priprema podatke za prikaz profila nekog
     * drugog korisnika
     */
    public function show()
    {
        $ser = new CimDbService();
        $user = $ser->getUserById($_GET['id_user']);
        $commentList = $ser->getMyComments($_GET['id_user']);

        $movie = array();
        foreach($commentList as $comment){
            $movie[] = $ser->getMovieById($comment->movie_id);
        }

        $wishList = $ser->getWishlist($_GET['id_user']);

        require_once __DIR__ . '/../view/userprofile.php';
    }

    /**
     * Funckija koja mijenja status korisnika
     */
    public function insert()
    {
      $ser = new CimDbService();
      $ser->updateStatus($_SESSION['user_id'],$_GET['movie_id'], $_GET['status']);

    }

    /**
    * Funckija koja briše film iz wishlista
    */
    public function remove()
    {
      $ser = new CimDbService();
      $ser->removeWish($_SESSION['user_id'],$_GET['movie_id']);

    }

    /**
     * Funckija koja je pozvana kad se neki korisnik
     * briše. Ovu mogućnost ima samo admin
     */
    public function removeuser()
    {
      $ser = new CimDbService();
      $ser->deleteUser($_GET['id_user']);

      $user = $ser->getUserById($_SESSION['user_id']);
      $commentList = $ser->getMyComments($_SESSION['user_id']);

      $movie = array();
      foreach($commentList as $comment){
          $movie[] = $ser->getMovieById($comment->movie_id);
      }

      $wishList = $ser->getWishlist($_SESSION['user_id']);
      $wish = $ser->getWishes($_SESSION['user_id']);
      $users = $ser->getAllUser($_SESSION['user_id']);

      require_once __DIR__ . '/../view/myprofile.php';
    }
};
?>
