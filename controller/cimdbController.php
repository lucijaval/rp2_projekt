<?php

/**
 * Razred čije se metode pozivaju odmah na početku aplikacije.
 * Funckija tog razreda šalje korisnika na prikaz za logiranje
 */
class cimdbController
{
    /**
     * Funckija koja preusmjerava korisnika na
     * stranicu/prikaz za logiranje
     */
    public function index()
    {
        // Samo preusmjeri na users podstranicu.
        header( 'Location: cimdb.php?rt=login' );
        exit();
    }
};

?>
