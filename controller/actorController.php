<?php

require_once __DIR__ . '/../model/cimdbservice.class.php';
/**
 * URL je oblika cimdb.php?rt=actor/show&id_actor=1
 */

/**
 * Razred actorController koji iz baze, pomoću funkcija iz razreda,
 * dohvaća informacije o glumcu i prikazuje profil glumca. Isto
 * tako, nudi funckiju koja ubacuje novog glumca u bazu.
 *
 * @see CimDbService
 */
class actorController
{
    /**
     * Funckija se poziva kad se želi prikazati
     * profil glumca
     */
    public function show()
    {
        $ser = new CimDbService();
        $actor = $ser->getActorById($_GET['id_actor']);
        $moviesList = $ser->getMoviesByActor($_GET['id_actor']);
        $rolesList = array();

        foreach ($moviesList as $movie){
            $role = $ser->getRole($_GET['id_actor'], $movie->id);
            $rolesList[$movie->id] = $role;
        }

        require_once __DIR__ . '/../view/actor.php';
    }

    /**
     * Funkcija se poziva kad se dodaje novi glumac  bazu podataka
     */
    public function add()
    {
        $ser = new CimDbService();
        $actor = $ser->addActor($_POST['name'], $_POST['surname'], $_POST['sex'], $_POST['birth_y'], $_POST['death_y'], $_POST['bio'], $_POST['photo']);

        $moviesList = $ser->getMoviesByActor($actor->id);
        $rolesList = array();

        foreach ($moviesList as $movie){
            $role = $ser->getRole($_GET['id_actor'], $movie->id);
            $rolesList[$movie->id] = $role;
        }
        require_once __DIR__ . '/../view/actor.php';
    }
};
?>
