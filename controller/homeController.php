<?php
require_once 'model/cimdbservice.class.php';

/**
 * Razred homeController priprema random 6 filmova
 * i priprema listu s njima kako bi ih se moglo prikazati
 * na home pageu
 */
class homeController
{
    /**
     * Funckija koja priprema filmove
     * i šalje korisnika na homepage
     */
    public function index()
    {
        $service = new CimDbService();

        $m = $service->getAllMovies();

        $movies = array();
        while(true){
          $rand = rand(0, sizeof($m)-1);
          $add = $m[$rand];
          if (!in_array($add, $movies)){
              $movies[] = $add;
              if(sizeof($movies)===6) break;
          }

        }

        require_once 'view/home.php';
    }
};

?>
