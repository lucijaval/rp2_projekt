<?php
require_once __DIR__.'/../model/cimdbservice.class.php';

/**
 * Razred registerController koji nudi funckije koje šalju
 * korisnika na stranicu koja sadrži formular za registraciju,
 * provjeravaju podatke registracije i rade registraciju korisnika.
 * Sve to rade uz pomoć funckija iz razreda @see CimDbService
 */
class registerController
{
    /**
     * Funckija koja šalje korisnika na stranicu
     * koja prikazuje formular za registraciju
     */
    public function index()
    {
        if( isset( $_GET['mess'] ) ) echo $_GET['mess'];
        if(!isset($_POST['register'])) require_once __DIR__.'/../view/register.php';
        else {
            $string = successful_register();
            if($string === 'good') {
                require_once __DIR__.'/../view/registered.php';
            }
            else header( 'Location: cimdb.php?rt=register&mess=' . $string );
        }
    }

    /**
     * Funckija koja provjerava unesene podatke, i jesu li
     * točni, točnije, može li se taj korisnik registrirati
     */
    public function check()
    {
        if( isset( $_GET['mess'] ) ) echo $_GET['mess'];

        if( !isset( $_GET['seq'] ) ) header( 'Location: cimdb.php?rt=register&mess=Something wrong with registration sequence.' );
        else {
            $ser = new CimDbService();
            $string2 = $ser->check_seq($_GET['seq']);
            if ($string2 === 'all_good') {
                require_once __DIR__ . '/../view/successfully_registered.php';
            } else header('Location: cimdb.php?rt=register&mess=' . $string2);
        }
    }
};

/**
 * Funckija provjerava da li je registracija uspješna ili ne.
 * Ako nije uspješna, prikazuje se odgovarajuća poruka
 * @return string
 *
 */
function successful_register() {
    if( !isset( $_POST['username'] ) || !isset( $_POST['password'] ) || !isset( $_POST['email'] )
        || !isset( $_POST['name'] ) || !isset( $_POST['surname'] ) )
        return 'Username, password, email, name or surname missing!' ;

    if( !preg_match( '/^[A-Za-z]{3,10}$/', $_POST['username'] ) )
        return 'Username must contain 3-10 letters or numbers.' ;

    else if( !filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL) )
        return 'E-mail adress not valid.' ;

    else {
        $ser = new CimDbService();

        return $ser->check_registration($_POST['username'], $_POST['password'], $_POST['name'], $_POST['surname'], $_POST['email']);
    }
}

?>